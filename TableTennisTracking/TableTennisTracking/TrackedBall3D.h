#pragma once

#include "TrackedBall2D.h"
#include "MeasurementCollection.h"
#include <list>
#include "utMeasurement/Measurement.h"

namespace TTT {

	class TrackedBall3D
	{
		// The balls, accessed by their cameraId
		std::vector<std::shared_ptr<TrackedBall2D>> balls2d_;
		// Indices that are set (i.e. cameras available)
		std::list<size_t> ballsAvailable_;
		// Timestamp of last used data, i.e. when trackers were synchronized
		time_t lastRefresh_;
		// Tracked 3D trajectory
		MeasurementCollection<Ubitrack::Math::Vector3d> trajectory_;
		
	public:
		TrackedBall3D(size_t numCameras);
		~TrackedBall3D();
		void setBall2d(int cameraId, std::shared_ptr<TrackedBall2D> ball2d);
		void removeBall2d(int cameraId);
		bool pending();
		bool synchronized();
		bool hasCamera(size_t cameraId);
		std::shared_ptr<TrackedBall2D> getBall2d(size_t idx);
		void addToTrajectory(Measurement<Ubitrack::Math::Vector3d> estimate3d);
		MeasurementCollection<Ubitrack::Math::Vector3d>& trajectory();
		// Calculates speed in meters per second based on trajectory
		float speed();

	};

}

