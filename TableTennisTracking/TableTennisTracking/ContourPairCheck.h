#pragma once

#include "ContourPair.h"
#include "Util.h"

namespace TTT {

	
	class ContourPairCheck
	{
	public:
		virtual ~ContourPairCheck() {};
		virtual ContourCheckResult check(std::shared_ptr<ContourPair> pair) = 0;
	};

	class RadiusCheck : public ContourPairCheck
	{
		// Maximum difference allowed in percent
		float maxRadiusChange_;

	public:
		RadiusCheck(float maxRadiusChange)
			: maxRadiusChange_(maxRadiusChange)
		{}

		virtual ContourCheckResult check(std::shared_ptr<ContourPair> pair) {
			
			if (abs(pair->to()->radius / pair->from()->radius - 1) > maxRadiusChange_)
				return RADIUS;

			return OK;
		};

	};

	class ProjectionRadiusCheck : public ContourPairCheck 
	{
		// Maximum difference allowed in percent
		float maxRadiusChange_;

	public:
		ProjectionRadiusCheck(float maxRadiusChange)
			: maxRadiusChange_(maxRadiusChange)
		{}
		
		virtual ContourCheckResult check(std::shared_ptr<ContourPair> pair) {
			ContourPairData<float> diameterProjections = pair->diameterProjections();
			if (abs(diameterProjections.to / diameterProjections.from - 1) > maxRadiusChange_)
				return RADIUS;

			return OK;
		}; 

	};

	class ProjectionLengthCheck : public ContourPairCheck
	{
		// Maximum difference allowed in percent
		float maxLengthChange_;

	public:
		ProjectionLengthCheck(float maxLengthChange)
			: maxLengthChange_(maxLengthChange)
		{}

		virtual ContourCheckResult check(std::shared_ptr<ContourPair> pair) {
			ContourPairData<float> lengthProjections = pair->lengthProjections();
			if (abs(lengthProjections.to / lengthProjections.from - 1) > maxLengthChange_)
				return LENGTH;

			return OK;
		};

	};

	class LowSpeedCheck : public ContourPairCheck
	{
		// Maximum difference allowed in percent
		float maxLengthChange_;

	public:
		LowSpeedCheck() {}

		virtual ContourCheckResult check(std::shared_ptr<ContourPair> pair) {
			ContourPairData<float> diameterProjections = pair->diameterProjections();
			// Less movement than 1/4 diameter
			if (pair->relativeSpeed() < ((diameterProjections.from + diameterProjections.to) / 2) / 4)
				return SPEED_LOW;

			return OK;
		};

	};

	class SpeedValidityCheck : public ContourPairCheck
	{
		// Camera exposure in seconds
		float exposure_;
		float timeBetweenFrames_;

	public:
		SpeedValidityCheck(float exposure, float timeBetweenFrames)
			: exposure_(exposure),
			timeBetweenFrames_(timeBetweenFrames)
		{}

		virtual ContourCheckResult check(std::shared_ptr<ContourPair> pair) {
			ContourPairData<float> diameterProjections = pair->diameterProjections();
			ContourPairData<float> lengthProjections = pair->lengthProjections();
			float avgDiameter = (diameterProjections.from + diameterProjections.to) / 2;
			float avgLength = (lengthProjections.from + lengthProjections.to) / 2;

			float minSpeed = (avgLength - avgDiameter) / exposure_ * timeBetweenFrames_ * 0.8f;
			float maxSpeed = (avgLength) / exposure_ * timeBetweenFrames_ * 1.1f;
			if (pair->relativeSpeed() < minSpeed) {
				//std::cout << "Speed: " << pair->relativeSpeed() << std::endl << "Min: " << minSpeed << std::endl << "Max: " << maxSpeed << std::endl;
				return SPEED_VALIDITY_LOW;
			}
			else if (pair->relativeSpeed() > maxSpeed){
				return SPEED_VALIDITY_HIGH;
			}

			return OK;
		};

	};

	class SpeedValidityCheckV2 : public ContourPairCheck
	{
		// Camera exposure in seconds
		float exposure_;

	public:
		SpeedValidityCheckV2(float exposure)
			: exposure_(exposure)
		{}

		virtual ContourCheckResult check(std::shared_ptr<ContourPair> pair) {
			float timeBetweenFrames = Util::nanosecondsToSeconds(pair->to()->frameTime - pair->from()->frameTime);
			ContourPairData<float> diameterProjections = pair->diameterProjections();
			ContourPairData<float> lengthProjections = pair->lengthProjections();
			float avgDiameter = (diameterProjections.from + diameterProjections.to) / 2;
			float avgLength = (lengthProjections.from + lengthProjections.to) / 2;
			float speed = (avgLength - avgDiameter);

			float minSpeed = (pair->distance() / timeBetweenFrames * exposure_ - avgDiameter) * 0.9;
			float maxSpeed = pair->distance() / timeBetweenFrames * exposure_ * 1.2f;
			if (speed < minSpeed) {
				return SPEED_VALIDITY_LOW;
			}
			else if (speed > maxSpeed) {
				return SPEED_VALIDITY_HIGH;
			}

			return OK;
		};

	};

	class LateralMovementCheck : public ContourPairCheck
	{
		// Minimum ratio between length and diameter before this check is performed.
		// If the ratio is small, the direction is inaccurate.
		float minLengthDiameterRatioForAngleCheck_;
		// Maximum angle difference
		float maxAngleDeviation_;

	public:
		LateralMovementCheck(float minLengthDiameterRatioForAngleCheck, float maxAngleDeviation)
			: minLengthDiameterRatioForAngleCheck_(minLengthDiameterRatioForAngleCheck),
			maxAngleDeviation_(maxAngleDeviation)
		{}

		virtual ContourCheckResult check(std::shared_ptr<ContourPair> pair) {
			bool checkLateral_from = pair->from()->length / pair->from()->diameter > minLengthDiameterRatioForAngleCheck_;
			bool checkLateral_to = pair->to()->length / pair->to()->diameter > minLengthDiameterRatioForAngleCheck_;
			if ((checkLateral_from && angleBetweenVectors(pair->from()->direction, pair->relativeDirectionNorm()) > maxAngleDeviation_) ||
				(checkLateral_to && angleBetweenVectors(pair->to()->direction, pair->relativeDirectionNorm()) > maxAngleDeviation_))
				//(checkLateral_to && checkLateral_from && angleBetweenVectors(p_to->direction, p_from->direction) > conf_.maxAngleDeviation))
				return LATERAL_MOVEMENT;

			return OK;
		};

	};

	class SimilarityCheck : public ContourPairCheck
	{
		double maxDifference_;

	public:
		SimilarityCheck(double maxDifference)
			: maxDifference_(maxDifference)
		{}

		virtual ContourCheckResult check(std::shared_ptr<ContourPair> pair) {
			double res = matchShapes(pair->from()->contour, pair->to()->contour, cv::ShapeMatchModes::CONTOURS_MATCH_I3, 0);
			if (res > maxDifference_) {
				//std::cout << "Similarity: " << res << std::endl;
				return SIMILARITY;
			}
				

			return OK;
		}
	};

}

