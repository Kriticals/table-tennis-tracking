#include "Contourpair.h"

using namespace cv;
using namespace std;

namespace TTT {

	ContourPair::ContourPair(shared_ptr<ContourProperties> from, shared_ptr<ContourProperties> to) :
		from_(from),
		to_(to),
		frameDifference_(to->frameId - from->frameId),
		difference_(to->massCenter - from->massCenter),
		relativeSpeed_(vectorLength(difference_)),
		lengthProjections_(&ContourPair::computeLengthProjections, *this),
		diameterProjections_(&ContourPair::computeDiameterProjections, *this)
	{
		// Normalize direction
		direction_ = difference_ / relativeSpeed_;
		// Velocity in pixels per second
		velocity_ = difference_ / deltaTime();
	}

	ContourPair::~ContourPair()
	{
	}

	std::shared_ptr<ContourProperties> ContourPair::from()
	{
		return from_;
	}

	std::shared_ptr<ContourProperties> ContourPair::to()
	{
		return to_;
	}

	cv::Vec2f ContourPair::relativeDirectionNorm()
	{
		return direction_;
	}

	cv::Vec2f ContourPair::relativeDirection()
	{
		return difference_;
	}

	cv::Vec2f ContourPair::velocity()
	{
		return velocity_;
	}

	float ContourPair::distance()
	{
		return sqrt(difference_.dot(difference_));
	}

	float ContourPair::relativeSpeed()
	{
		return relativeSpeed_;
	}

	float ContourPair::deltaTime()
	{
		return (to_->frameTime - from_->frameTime) / 1000000000.0f;
	}

	float ContourPair::speed()
	{
		return relativeSpeed_ / deltaTime();
	}

	ContourPairData<float> ContourPair::lengthProjections()
	{
		return lengthProjections_;
	}

	ContourPairData<float> ContourPair::diameterProjections()
	{
		return diameterProjections_;
	}

	ContourPairData<float> ContourPair::computeLengthProjections()
	{
		ContourPairData<float> lengthProjections;
		lengthProjections.from = projectionLength(from_->contour, direction_);
		lengthProjections.to = projectionLength(to_->contour, direction_);
		return lengthProjections;
	}

	ContourPairData<float> ContourPair::computeDiameterProjections()
	{
		ContourPairData<float> diameterProjections;
		Vec2f directionNormal = normalVector(direction_);
		diameterProjections.from = projectionLength(from_->contour, directionNormal);
		diameterProjections.to = projectionLength(to_->contour, directionNormal);
		return diameterProjections;
	}


}