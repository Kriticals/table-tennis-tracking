#pragma once

#include "TableTennisTracking2D.h"
#include <fstream>
#include <map>
#include "TrackedBall3D.h"
#include "Util.h"
#include <utFacade/AdvancedFacade.h>
#include <utVision/Image.h>
#include <utMath/Geometry/PointProjection.h>
#include "EvaluationData.h"

namespace TTT {

	enum TrackingMode {
		LIVE,
		OFFLINE
	};

	class TableTennisTracking3D : public TrackingObserver2D
	{
		const size_t numCameras_;
		const TrackingMode trackingMode_;
		const std::string recordingBaseFolder_;
		const bool referenceDataAvailable_;

		const float maxReprojectionError_;
		
		std::vector<TableTennisTracking2D> trackers_;
		std::vector<std::ifstream> recordingFiles_;
		std::vector<cv::VideoCapture> videoCaptures_;
		std::map<size_t, std::shared_ptr<TrackedBall3D>> trackedBalls_;
		std::map<size_t, bool> inPendingBalls_;
		std::vector<std::list<std::shared_ptr<TrackedBall2D>>> pendingBalls;
		Ubitrack::Measurement::CameraIntrinsics intrinsics_cam1_;
		Ubitrack::Measurement::CameraIntrinsics intrinsics_cam2_;
		Ubitrack::Measurement::Matrix3x3 intrinsics_cam1_undist_;
		Ubitrack::Measurement::Matrix3x3 intrinsics_cam2_undist_;
		Ubitrack::Measurement::Pose extrinsics_cam2;
		std::vector<cv::UMat> frames;
		boost::shared_ptr<Ubitrack::Components::ApplicationPushSource<Ubitrack::Measurement::ImageMeasurement> > imageComp;
		boost::shared_ptr<Ubitrack::Components::ApplicationPushSource<Ubitrack::Measurement::Position> > positionComp;
		boost::shared_ptr<Ubitrack::Components::ApplicationPushSink<Ubitrack::Measurement::ImageMeasurement>> leftCameraSink;
		boost::shared_ptr<Ubitrack::Components::ApplicationPushSink<Ubitrack::Measurement::ImageMeasurement>> rightCameraSink;

		time_t currentTimestamp_;

		size_t counterrr = 0;
		

	public:
		TableTennisTracking3D(std::string recordingBaseFolder, bool referenceDataAvailable = false);
		~TableTennisTracking3D();
		virtual void onBallTracked(const std::shared_ptr<TrackedBall2D> ball, TableTennisTracking2D* tracker) override;
		virtual void onBallBounce(const std::shared_ptr<TrackedBall2D> ball, cv::Vec2f bounceLocation, TableTennisTracking2D * tracker) override;

		EvaluationData evaluations;

	private:
		bool startUbitrackDataflow();
		void processRecording();
		void processCameraFeed();
		void afterFrameProcessing();
		bool tryCompute3Destimate(std::shared_ptr<TrackedBall3D> ball, Ubitrack::Math::Vector3d& out, double &reprojectionError);
		bool tryMatchBall(std::shared_ptr<TrackedBall2D> trackedBall, size_t cameraId);
		
	};

	void onCameraImageLeft(Ubitrack::Measurement::ImageMeasurement image);
	void onCameraImageRight(Ubitrack::Measurement::ImageMeasurement image);
}

