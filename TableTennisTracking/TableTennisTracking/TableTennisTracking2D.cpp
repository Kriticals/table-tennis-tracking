#include "TableTennisTracking2D.h"


namespace TTT {

	using namespace std;

	using namespace cv;

	TableTennisTracking2D::TableTennisTracking2D(size_t cameraId) :
		cameraId_(cameraId),
		frameCounter_(0),
		keyboard_(0),
		updateBackground_(true),
		backgroundSubtractor_(createBackgroundSubtractorMOG2(120, 16.0)),
		orientationApproximator_(MIN_AREA_RECT),
		conf_(Configuration::instance()),
		frameDataList_(0)
	{
		conf_.drawDebug = true;
		kernelOpen_ = getStructuringElement(MorphShapes::MORPH_RECT,
			Size(2 * openSize_ + 1, 2 * openSize_ + 1),
			Point(openSize_, openSize_));

		// For debugging only
		//namedWindow("Output Tracker 2D [" + std::to_string(cameraId_) + "]");
		//setMouseCallback("Output Tracker 2D [" + std::to_string(cameraId_) + "]", onMouseDetection, this);

		detectionChecks_ = std::vector<std::shared_ptr<ContourPairCheck>>{
			make_shared<ProjectionRadiusCheck>(conf_.maxRadiusChange),
			make_shared<ProjectionLengthCheck>(conf_.maxLengthChange),
			make_shared<LowSpeedCheck>(),
			make_shared<SpeedValidityCheck>(conf_.exposure, conf_.timeBetweenFrames),
			//make_shared<SpeedValidityCheckV2>(conf_.exposure),
			make_shared<LateralMovementCheck>(conf_.minLengthDiameterRatioForAngleCheck, conf_.maxAngleDeviation),
			//make_shared<SimilarityCheck>(conf_.maxSimilarityDeviation)
		};

		trackingChecks_ = std::vector<std::shared_ptr<ContourPairCheck>>{
			make_shared<ProjectionRadiusCheck>(conf_.maxRadiusChange),
			make_shared<ProjectionLengthCheck>(conf_.maxLengthChange),
		};

		changeState(BACKGROUND_INITIALIZATION);
	}

	void TableTennisTracking2D::changeState(TTTState newState)
	{
		// Do something before leaving state
		switch (state_) {
		case BALL_INITIALIZATION:
			updateBackground_ = true;
			break;
		}

		// Do something on entering newState
		switch (newState) {
		case BALL_INITIALIZATION:
			updateBackground_ = false;
			break;
		}

		state_ = newState;
	}

	void TableTennisTracking2D::handleFrame(UMat input, time_t time)
	{
		frameCounter_++;
		// Get last frames data
		shared_ptr<FrameData> lastFrameData = frameCounter_ > 1 ? frameDataList_.back() : make_shared<FrameData>();

		//Create new frame data
		shared_ptr<FrameData> newFrameData = make_shared<FrameData>();
		newFrameData->id = frameCounter_;
		newFrameData->time = time;
		newFrameData->input = input;
		input.copyTo(newFrameData->output);
		frameDataList_.push_back(newFrameData);

		//Background substraction, opening of foreground mask
		preprocessing(newFrameData);

		switch (state_) {

			// Build a background model at startup before trying any detection/tracking
		case BACKGROUND_INITIALIZATION:
			if (frameCounter_ >= conf_.initializationFrameCount) {
				changeState(TRACKING);
			}
			break;

			// Main detector loop
		case TRACKING:
			findBallCandidates(newFrameData);
			trackBalls(newFrameData);
			detectBalls(lastFrameData, newFrameData);
			break;
		}

		// Remove old frame data
		if (frameDataList_.size() > 5)
			frameDataList_.pop_front();


		if (conf_.drawDebug) {
			//drawCandidates(newFrameData->output, *newFrameData, Scalar(255, 0, 0));

			/*Mat test;// = Mat(newFrameData->output.rows, newFrameData->output.cols, newFrameData->output.type());
			newFrameData->output.copyTo(test);
			if(frameDataList_.size() > 1)
				drawContours(test, frameDataList_[frameDataList_.size() - 2]->candidateContours, -1, Scalar(0, 0, 255), 1);
			drawContours(test, newFrameData->candidateContours, -1, Scalar(0, 255, 0), 1);*/
			//imwrite("C:/ubitrack/outputimages/New folder/outputCandidates.png", test);
			for (shared_ptr<TrackedBall2D> ball : trackedBalls()) {
				if (ball->detections().size() > 3) {
					drawTrackedBall(newFrameData->output, ball, TTT::TrackedBallDrawMode::LINE_VERTICES, Scalar(0, 255, 0), Scalar(200, 200, 200), Scalar(0, 255, 255));
					drawTrackedBall(newFrameData->foregroundMaskColor, ball, TTT::TrackedBallDrawMode::CONTOUR);
				}
			}
			imshow("Output Tracker 2D [" + std::to_string(cameraId_) + "]", newFrameData->output);
		}

		//imshow("Output Tracker 2D [" + std::to_string(cameraId_) + "]", newFrameData->output);
	}

	void TableTennisTracking2D::preprocessing(shared_ptr<FrameData> frameData)
	{
		// Bluring reduces noise, but is fairly expensive
		//blur(input, input, Size(3, 3));

		backgroundSubtractor_->apply(frameData->input, frameData->foregroundMaskUnopened, updateBackground_ ? -1 : 0);
		threshold(frameData->foregroundMaskUnopened, frameData->foregroundMaskUnopened, 254, 255, THRESH_BINARY);
		if (conf_.drawDebug) {
			imshow("FGMask" + cameraId_, frameData->foregroundMaskUnopened);
		}
		morphologyEx(frameData->foregroundMaskUnopened, frameData->foregroundMask, MORPH_OPEN, kernelOpen_, Point(-1, -1), 1);
		cvtColor(frameData->foregroundMask, frameData->foregroundMaskColor, cv::COLOR_GRAY2BGR);

		if (conf_.drawDebug) {
			imshow("FGMask opened" + cameraId_, frameData->foregroundMask);
		}
	}

	void TTT::TableTennisTracking2D::drawCandidates(UMat & input, FrameData & result, Scalar color)
	{
		drawContours(input, result.candidateContours, -1, color);
	}

	bool TableTennisTracking2D::findBallCandidates(shared_ptr<FrameData> frameData)
	{
		// Extract contours from foreground mask
		findContours(frameData->foregroundMask, frameData->contours, RetrievalModes::RETR_LIST, ContourApproximationModes::CHAIN_APPROX_SIMPLE);

		// Filter contours
		for (int i = 0; i < frameData->contours.size(); ++i) {
			// Ignore contours with less than 6 contour points
			if (frameData->contours[i].size() < 6)
				continue;

			// Get contour properties like length, diameter, estimated speed and orientation
			shared_ptr<ContourProperties> properties = make_shared<ContourProperties>(frameData->contours[i], frameData->id, frameData->time, orientationApproximator_);
			// Roughly check if contour could be a ball
			ContourCheckResult r = contourValidityCheck(properties);
			if (r == OK) {
				// Save all candidate contours in one array for debug output
				frameData->candidateContours.push_back(frameData->contours[i]);
				// Contour is considered a ball-candidate, save for detection check
				frameData->candidates.push_back(properties);
			}
		}

		// Any candidates found?
		return frameData->candidates.size() > 0;
	}

	void TableTennisTracking2D::detectBalls(shared_ptr<FrameData> lastFrame, shared_ptr<FrameData> currentFrame)
	{
		// Check for new detections
		for (auto i = lastFrame->candidates.begin(); i != lastFrame->candidates.end(); i++) {
			// Previous frame's contour
			shared_ptr<ContourProperties> contourPrevious = *i;
			if (contourPrevious->assignedToBall == true)
				continue;

			for (auto j = currentFrame->candidates.begin(); j != currentFrame->candidates.end(); j++) {
				// Current frame's contour
				shared_ptr<ContourProperties> contourCurrent = *j;
				if (contourCurrent->assignedToBall == true)
					continue;

				// Perform all pre-defined checks that evaluate whether two contours match
				shared_ptr<ContourPair> pair = make_shared<ContourPair>(contourPrevious, contourCurrent);
				ContourCheckResult res = performContourChecks(pair, detectionChecks_);
				if (res == OK) {
					// Two contours matched, so add a tracked ball
					shared_ptr<TrackedBall2D> newBall = make_shared<TrackedBall2D>();
					newBall->addPair(pair);
					
					// TODO: Refactor to function
					// Check if this might be a re-detection from a previously tracked ball; estimate bounce location
					for (shared_ptr<TrackedBall2D> trackedBall : trackedBalls_) {
						if (trackedBall->missedFrames() >= 1 && trackedBall->detections().size() > 4) {
							shared_ptr<ContourProperties> contourProperties = trackedBall->detections().back();
							shared_ptr<ContourPair> trackedPair = trackedBall->trackingPairs().back();
							Vec2f lastPos = contourProperties->massCenter;
							Vec2f lastDirection = trackedPair->relativeDirectionNorm();
							Vec2f intersection;
							if (vectorIntersection(lastPos, lastDirection, contourPrevious->massCenter, pair->relativeDirectionNorm(), intersection)) {
								float timeDiff = (currentFrame->time - contourProperties->frameTime) / 1000000000.0f;
								float lengthToIntersection = vectorLength(lastPos - intersection);
								float pathLength = lengthToIntersection + vectorLength(intersection - Vec2f(contourPrevious->massCenter));
								// Check if trackedBall could have reached this location in time
								if (pathLength < timeDiff * trackedPair->speed()) {
									circle(currentFrame->output, Point(intersection), 4, Scalar(0, 255, 0), -1);
									line(currentFrame->output, Point(lastPos), Point(lastPos + lastDirection * trackedPair->relativeSpeed() * 5), Scalar(0, 0, 255));
									line(currentFrame->output, Point(pair->from()->massCenter), Point(Vec2f(pair->from()->massCenter) - pair->relativeDirectionNorm() * pair->relativeSpeed() * 5), Scalar(0, 0, 255));
									// Guess timestamp at intersection
									time_t timeAtIntersection = trackedPair->to()->frameTime + (lengthToIntersection / trackedPair->speed()) * 1000000000;
									// TODO: report intersection/bounce to listeners
								}
							}
						}
					}

					trackedBalls_.push_back(newBall);

					break;
				}
			}
		}
	}

	void TableTennisTracking2D::trackBalls(shared_ptr<FrameData> currentFrame) {
		// Check if any of the already tracked balls match one of found contours
		for (shared_ptr<TrackedBall2D> trackedBall : trackedBalls_) {
			shared_ptr<ContourProperties> p_from = trackedBall->detections().back();
			float timeDiff = (currentFrame->time - p_from->frameTime) / 1000000000.0f;
			Vec2f predictedLocation = trackedBall->predictNextLocation(timeDiff, 5);
			trackedBall->predictions().push_back(predictedLocation);
			//circle(currentFrame->output, Point(predictedLocation), 3, Scalar(255, 0, 0), -1);
			bool missed = true;
			for (auto it = currentFrame->candidates.begin(); it != currentFrame->candidates.end(); it++) {
				shared_ptr<ContourProperties> p_to = *it;
				if (p_to->assignedToBall == true)
					continue;

				// Require 3 detections before running the tracking algorithm
				if (trackedBall->detections().size() > 2) {
					// Calculate the error of the prediction
					Vec2f predictionDir = predictedLocation - Vec2f(p_to->massCenter);
					float distToPrediction = vectorLength(predictionDir);
					// If distance to prediction is smaller than radius, or the prediction is inside the contour, continue
					if (distToPrediction < p_from->radius || cv::pointPolygonTest(p_to->contour, predictedLocation, false) >= 0) {
						shared_ptr<ContourPair> pair = make_shared<ContourPair>(p_from, p_to);
						// Contour is matched to the tracked ball if these additional contour pair checks are successful
						if (performContourChecks(pair, trackingChecks_) == OK) {
							trackedBall->addPair(pair);
							trackedBall->predictionsSuccess().push_back(true);
							missed = false;
							break;
						}
					}
				}
				else { // Run detection checks for another frame for newly tracked balls
					shared_ptr<ContourPair> pair = make_shared<ContourPair>(p_from, p_to);
					if (performContourChecks(pair, detectionChecks_) == OK) {
						trackedBall->addPair(pair);
						trackedBall->predictionsSuccess().push_back(true);
						missed = false;
						break;
					}
				}


			}
			if (missed) {
				trackedBall->addMissedFrame();
				trackedBall->predictionsSuccess().push_back(false);
			}
			else {
				// Report to observers that the ball has been tracked
				for (TrackingObserver2D* observer : observers_)
					observer->onBallTracked(trackedBall, this);
			}
		}

		// Remove tracked balls that haven't been tracked for more than 3 frames
		trackedBalls_.remove_if([](shared_ptr<TrackedBall2D> ball) { return ball->missedFrames() > 3; });
	}


	ContourCheckResult TableTennisTracking2D::performContourChecks(const shared_ptr<ContourPair> pair, const vector<shared_ptr<ContourPairCheck>>& checks)
	{
		for (shared_ptr<ContourPairCheck> check : checks) {
			ContourCheckResult r = check->check(pair);
			if (r != OK)
				return r;
		}
		return OK;
	}

	// TO DO: Split into multiple functions, debug only
	void TableTennisTracking2D::onMouseInternal(int event, int x, int y, int)
	{
		if (frameDataList_.size() < 2)
			return;

		shared_ptr<FrameData> dResultPrev = frameDataList_[frameDataList_.size() - 2];
		shared_ptr<FrameData> dResultCurr = frameDataList_.back();

		if (event == MouseEventTypes::EVENT_LBUTTONDOWN) {
			if (selectedContour1_ != -1 && selectedContour2_ != -1) {
				selectedContour1_ = -1;
				selectedContour2_ = -1;
			}

			if (selectedContour1_ == -1) {
				selectedContour1_ = findDetectionAtPosition(x, y, dResultPrev);
				if (selectedContour1_ != -1) {
					cout << "----------------------------\n";
					cout << "Previous Contour selected for Detection Check!\n";
				}
				else {
					cout << "Couldn't find Contour.\n";
				}
			}
			else if (selectedContour2_ == -1) {
				selectedContour2_ = findDetectionAtPosition(x, y, dResultCurr);
				if (selectedContour2_ != -1) {
					cout << "----------------------------\n";
					cout << "Current Contour selected for Detection Check!\n";
				}
				else {
					cout << "Couldn't find Contour.\n";
				}
			}

			if (selectedContour1_ != -1 && selectedContour2_ != -1) {
				shared_ptr<ContourPair> pair = make_shared<ContourPair>(dResultPrev->candidates[selectedContour1_], dResultCurr->candidates[selectedContour2_]);
				ContourCheckResult r = performContourChecks(pair, detectionChecks_);
				cout << "----------------------------\n";
				cout << "|      Detection Check     |\n";
				cout << "----------------------------\n";
				cout << "| Result: " << DetectionCheckResult2String(r) << endl;
				cout << "----------------------------\n";
				dResultPrev->candidates[selectedContour1_]->print();
				cout << "----------------------------\n";
				dResultCurr->candidates[selectedContour2_]->print();
				cout << "----------------------------\n";

				selectedContour1_ = -1;
				selectedContour2_ = -1;
			}
		}

		if (event == MouseEventTypes::EVENT_RBUTTONDOWN) {
			for (int i = 0; i < dResultCurr->contours.size(); i++) {
				if (pointPolygonTest(dResultCurr->contours[i], Point2f(x, y), false) > 0) {
					ContourProperties p(dResultCurr->contours[i], dResultCurr->id, dResultCurr->time);
					ContourCheckResult r = contourValidityCheck(make_shared<ContourProperties>(p));
					p.print();
					cout << "Contour Check Result: " << DetectionCheckResult2String(r) << endl;
					break;
				}
			}
		}
	}

	int TableTennisTracking2D::findDetectionAtPosition(int x, int y, shared_ptr<FrameData> frameData)
	{
		for (int i = 0; i < frameData->candidateContours.size(); ++i) {
			if (pointPolygonTest(frameData->candidateContours[i], Point2f(x, y), false) > 0) {
				return i;
			}
		}

		return -1;
	}

	string TableTennisTracking2D::DetectionCheckResult2String(ContourCheckResult r)
	{
		switch (r) {
		case OK:
			return "OK";
			break;
		case FAIL:
			return "FAIL";
			break;
		case RADIUS:
			return "RADIUS";
		case LENGTH:
			return "LENGTH";
			break;
		case SPEED_LOW:
			return "SPEED_LOW";
			break;		
		case SPEED_VALIDITY_LOW:
			return "SPEED_VALIDITY_LOW";
			break;		
		case SPEED_VALIDITY_HIGH:
			return "SPEED_VALIDITY_HIGH";
			break;
		case SIMILARITY:
			return "SIMILARITY";
			break;
		case LATERAL_MOVEMENT:
			return "LATERAL_MOVEMENT";
			break;
		case ContourCheckResult::AREA_TOO_SMALL:
			return "AREA_TOO_SMALL";
			break;
		case ContourCheckResult::AREA_TOO_BIG:
			return "AREA_TOO_BIG";
			break;
		case ContourCheckResult::AREA_MODEL_DEVIATION:
			return "AREA_MODEL_DEVIATION";
			break;
		case ContourCheckResult::ALREADY_TRACKED:
			return "ALREADY_TRACKED";
			break;
		}

		return "STRING CONVERSION FOR ENUM MISSING!";
	}

	bool TableTennisTracking2D::getFrameData(FrameData & result)
	{
		result = *frameDataList_.back();
		return result.candidates.size() > 0;
	}

	list<shared_ptr<TrackedBall2D>>& TableTennisTracking2D::trackedBalls()
	{
		return trackedBalls_;
	}

	void TableTennisTracking2D::addObserver(TrackingObserver2D * observer)
	{
		if (std::find(observers_.begin(), observers_.end(), observer) != observers_.end())
			return;

		observers_.push_back(observer);
	}

	void TableTennisTracking2D::drawOutput(bool val)
	{
		conf_.drawDebug = val;
	}

	void TableTennisTracking2D::addText(cv::Point position, std::string text, cv::Scalar color)
	{
		auto lastFrameData = frameDataList_.back();
		cv::putText(lastFrameData->output, text, position, FONT_HERSHEY_DUPLEX, 1, color);
	}

	void TableTennisTracking2D::drawCircle(cv::Point position, cv::Scalar color)
	{
		auto lastFrameData = frameDataList_.back();
		cv::circle(lastFrameData->output, position, 4, color);
	}

	void onMouseDetection(int event, int x, int y, int i, void * userData)
	{
		TTT::TableTennisTracking2D *ttt = (TTT::TableTennisTracking2D*)(userData);
		ttt->onMouseInternal(event, x, y, i);
	}

}