#pragma once

template <typename T>
class Lazy {

	std::function<T()> initializer_;
	mutable T value_;
	mutable bool initialized_;

public:
	Lazy(std::function<T()> initializer)
		: initializer_(std::move(initializer)),
		initialized_(false)
	{}

	operator T &() {
		return get();
	}

	T& get() {
		if (!initialized_) {
			value_ = initializer_();
			initialized_ = true;
		}
		return value_;
	}

};

template <typename T, typename C>
class LazyMember {

	typedef T(C::* MemberFunctionPointer) ();
	MemberFunctionPointer initializer_;
	C &instance_;
	mutable T value_;
	mutable bool initialized_;

public:
	LazyMember(MemberFunctionPointer initializer, C &instance)
		: initializer_(initializer),
		instance_(instance),
		initialized_(false)
	{}

	operator T const &() const {
		return get();
	}
	T const & get() const {
		if (!initialized_) {
			value_ = (instance_.*initializer_)();
			initialized_ = true;
		}
		return value_;
	}

};