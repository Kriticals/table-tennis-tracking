#include "EvaluationData.h"



EvaluationData::EvaluationData()
{
}


EvaluationData::~EvaluationData()
{
}


void EvaluationData::addTrackedData(TTT::Measurement<Ubitrack::Math::Vector3d> position3d, double reprojectionError, double absoluteError, Ubitrack::Math::Vector3d error)
{
	trackingData_.addMeasurement(position3d);
	reprojectionErrors.addMeasurement({ position3d.time(), reprojectionError });
	absoluteErrors.addMeasurement({ position3d.time(), absoluteError });
	errors.addMeasurement({ position3d.time(), error });
}
