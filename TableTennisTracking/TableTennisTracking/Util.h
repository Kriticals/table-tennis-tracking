#pragma once

#ifndef __UBITRACK_UTIL_CALIBFILE_H_INCLUDED__
#include <utUtil/CalibFile.h>
#endif
#include <utMeasurement/Measurement.h>
#include <utAlgorithm/3DPointReconstruction.h>

#include <opencv2\core.hpp>

#include <boost/numeric/ublas/operation.hpp>

#include "MeasurementCollection.h"

namespace TTT {

	class Util {

	public:
		static float nanosecondsToSeconds(time_t t);

		static time_t secondsToNanoseconds(float t);

		static time_t milliToNanoseconds(time_t milli);

		static float clip(float n, float lower, float upper);

		template<typename T>
		static void minMax(const T* a, size_t n, T& min, T& max);

		static Ubitrack::Math::Vector2d convertToUbitrackCoordinates(cv::Vec2f p1, int imageHeight);

		static double calculateReprojectionError(Ubitrack::Math::Matrix3x3d intrinsics, Ubitrack::Math::Vector3d estimate, Ubitrack::Math::Vector2d p_original, Ubitrack::Math::Vector2d &p_projected_out);

		static Ubitrack::Measurement::CameraIntrinsics loadCameraIntrinsics(const std::string file);

		static Ubitrack::Measurement::Matrix3x3 loadMatrix(const std::string file);

		static Ubitrack::Measurement::Pose loadCameraExtrinsics(const std::string file);

		static MeasurementCollection<Ubitrack::Math::Vector3d> loadReferencePositionData(const std::string fil, const std::string ts_file, time_t offset_ns = 0);

		static Ubitrack::Math::Vector3d compute3DPosition(Ubitrack::Math::Matrix3x3d intrinsics_cam1, Ubitrack::Math::Vector2d p_cam1, Ubitrack::Math::Matrix3x3d intrinsics_cam2, Ubitrack::Math::Pose extrinsics_cam2, Ubitrack::Math::Vector2d p_cam2);

		static Ubitrack::Math::Vector2d project3Dto2D(Ubitrack::Math::Matrix3x3d intrinsics, Ubitrack::Math::Pose extrinsics, Ubitrack::Math::Vector3d X);

		static double round(double x, int n);

		static float convertSpeedMpsToKmh(float metersPerSecond);

	};

	template<typename T>
	inline void Util::minMax(const T * a, size_t n, T & min, T & max)
	{
		if (n == 0)
			return;

		min = a[0];
		max = a[0];
		for (size_t i = 1; i < n; ++i) {
			if (a[i] < min)
				min = a[i];
			else if (a[i] > max)
				max = a[i];
		}
	}

}