#pragma once

class Configuration
{
public:
	// Number of frames used to create the background model before starting actual tracking
	int initializationFrameCount;
	// Ball size in cm (Ping pong balls are 40mm in diameter)
	float realBallSize;
	float fps;
	float timeBetweenFrames;
	float exposure; //ms
	float circularRatioDeviation;
	float minContourArea;
	float maxContourArea;
	float areaModelFactor;
	float maxAreaRatioDeviation;
	float maxSpeedChange;
	float maxRadiusChange;
	float maxLengthChange;
	float minLengthDiameterRatioForAngleCheck;
	float maxAngleDeviation;
	float searchRadiusSpeedFactor;
	float maxSimilarityDeviation;

	bool drawDebug;

	static Configuration& instance()
	{
		static Configuration _instance;
		return _instance;
	}
	~Configuration() {}
	
private:
	Configuration() {
		InitializeDefaultValues();
	}
	Configuration(const Configuration&); 
	Configuration & operator = (const Configuration &); 

	void InitializeDefaultValues() {
		initializationFrameCount = 5;
		
		realBallSize = 0.04f;
		
		fps = 60.158f;
		//fps = 30;
		timeBetweenFrames = 1 / fps;
		//exposure = timeBetweenFrames;
		exposure = 5.0f / 1000.0f; //ms
		//exposure = 1.0 / 80;
		//exposure = 1.0 / 30;
		circularRatioDeviation = 0.2f;
		//minContourArea = 15.0f;
		minContourArea = 15.0f;
		maxContourArea = 14000.0f;
		areaModelFactor = 1;
		maxAreaRatioDeviation = 0.3f;
		maxSpeedChange = 0.5f;
		maxRadiusChange = 0.25f;
		maxLengthChange = 0.3f;
		minLengthDiameterRatioForAngleCheck = 1.5f;
		maxAngleDeviation = 20.0f;
		searchRadiusSpeedFactor = 3.0f;
		maxSimilarityDeviation = 0.35f;

	}
};