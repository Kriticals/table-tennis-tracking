#pragma once

#include "ContourProperties.h"
#include <opencv2\core.hpp>
#include "Lazy.h"

namespace TTT {

	template <typename T>
	struct ContourPairData {
		T from;
		T to;
	};

	class ContourPair
	{
		// The contour coming from
		std::shared_ptr<ContourProperties> from_;
		// The contour moving to
		std::shared_ptr<ContourProperties> to_;
		// The vector from one center position to the other
		cv::Vec2f difference_;
		// Normalized difference_
		cv::Vec2f direction_;
		// This is the velocity in pixels per second
		cv::Vec2f velocity_;
		// Length of difference_
		float relativeSpeed_;
		// Number of frames missing in between the two
		int frameDifference_;
		// Projection length of contour along direction_
		LazyMember<ContourPairData<float>, ContourPair> lengthProjections_;
		// Projection length of contour along normal of direction_ 
		LazyMember<ContourPairData<float>, ContourPair> diameterProjections_;

	public:
		ContourPair(std::shared_ptr<ContourProperties> from, std::shared_ptr<ContourProperties> to);
		~ContourPair();

		std::shared_ptr<ContourProperties> from();
		std::shared_ptr<ContourProperties> to();
		cv::Vec2f relativeDirectionNorm();
		cv::Vec2f relativeDirection();
		cv::Vec2f velocity();
		float distance();
		float relativeSpeed();
		// Time difference in seconds
		float deltaTime();
		float speed();
		ContourPairData<float> lengthProjections();
		ContourPairData<float> diameterProjections();

	private:
		ContourPairData<float> computeLengthProjections();
		ContourPairData<float> computeDiameterProjections();

	};

}

