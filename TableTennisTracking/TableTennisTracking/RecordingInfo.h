#pragma once
#include <string>

namespace TTT {



	struct RecordingInfo
	{
		std::string baseFolder;
		double exposure;
		double fps;
		time_t throwStartTime;
		time_t throwEndTime;

	public:
		RecordingInfo(std::string baseFolder, double exposure, double fps, time_t throwStartTime, time_t throwEndTime);
		~RecordingInfo();
	};



}