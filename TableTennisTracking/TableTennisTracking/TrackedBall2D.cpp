#include "TrackedBall2D.h"



namespace TTT {

	using namespace std;
	using namespace cv;

	unsigned int TrackedBall2D::s_idCounter = 0;

	TrackedBall2D::TrackedBall2D() :
		missedFrames_(0),
		Id(s_idCounter++)
	{
		
	}

	TrackedBall2D::~TrackedBall2D()
	{
		
	}

	void TrackedBall2D::addDetection(shared_ptr<ContourProperties> detection)
	{
		if (detections_.size() > 0)
			trackingPairs_.push_back(make_shared<ContourPair>(detections_.back(), detection));
		detections_.push_back(detection);
		detection->assignedToBall = true;
		missedFrames_ = 0;
	}

	void TrackedBall2D::addPair(shared_ptr<ContourPair> pair)
	{
		trackingPairs_.push_back(pair);

		if (detections_.size() == 0) {
			detections_.push_back(pair->from());
			pair->from()->assignedToBall = true;
		}
			
		detections_.push_back(pair->to());
		pair->to()->assignedToBall = true;
		missedFrames_ = 0;
	}

	void TrackedBall2D::addMissedFrame()
	{
		missedFrames_++;
	}

	std::vector<shared_ptr<ContourProperties>>& TrackedBall2D::detections()
	{
		return detections_;
	}

	std::vector<std::shared_ptr<ContourPair>>& TrackedBall2D::trackingPairs()
	{
		return trackingPairs_;
	}

	std::vector<cv::Vec2f>& TrackedBall2D::predictions()
	{
		return predictions_;
	}

	std::vector<bool>& TrackedBall2D::predictionsSuccess()
	{
		return predictionsSuccess_;
	}

	std::shared_ptr<ContourProperties> TrackedBall2D::getLastDetection()
	{
		return detections_.back();
	}

	int TrackedBall2D::missedFrames()
	{
		return missedFrames_;
	}

	cv::Vec2f TrackedBall2D::predictNextLocation(float timestep, int numSteps = 1)
	{
		float step = timestep / numSteps;
		size_t numPairs = trackingPairs_.size();
		if (numPairs > 0) {
			shared_ptr<ContourPair> pair1 = trackingPairs_[numPairs > 1 ? numPairs - 2 : numPairs - 1];
			shared_ptr<ContourPair> pair2 = trackingPairs_[numPairs - 1];

			// Calculate time difference between pairs
			float pairTimeDiff = ((pair2->to()->frameTime + pair2->from()->frameTime) / 2 - (pair1->to()->frameTime + pair1->from()->frameTime) / 2) / 1000000000.0f;
			
			Vec2f acceleration = (pair2->velocity() - pair1->velocity());
			if (pairTimeDiff != 0) {
				acceleration /= pairTimeDiff;
			}
			 
			Vec2f velocity = pair2->velocity();
			Vec2f location = Vec2f(pair2->to()->massCenter);
			for (size_t i = 0; i < numSteps; ++i) {
				velocity += acceleration * step;
				location += velocity * step;
			}
			return location;
		}
		
		return NULL;
	}


}
