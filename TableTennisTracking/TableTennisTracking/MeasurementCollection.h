#pragma once

#include <set>
#include "Measurement.h"

namespace TTT {

	template <typename T>
	class MeasurementCollection
	{
		
	public:
		std::set<Measurement<T>> measurements_;
		MeasurementCollection();
		~MeasurementCollection();
		void addMeasurement(const Measurement<T> m);
		bool TryGetMeasurement(time_t atTime, time_t maxDiff, Measurement<T> & out);
	};

	template<typename T>
	inline MeasurementCollection<T>::MeasurementCollection()
	{
	}

	template<typename T>
	inline MeasurementCollection<T>::~MeasurementCollection()
	{
	}

	template<typename T>
	inline void MeasurementCollection<T>::addMeasurement(const Measurement<T> m)
	{
		measurements_.insert(m);
	}

	template<typename T>
	inline bool MeasurementCollection<T>::TryGetMeasurement(time_t atTime, time_t maxDiff, Measurement<T> & out)
	{
		Measurement<T> before, after;
		for (Measurement<T> m : measurements_) {
			if (m.time() < atTime) {
				before = m;
			}
			else if (m.time() > atTime) {
				after = m;
				break;
			}
			else {
				out = m;
				return true;
			}
		}

		if (before.time() >= 0 && after.time() >= 0) {
			if (after.time() - before.time() > maxDiff)
				return false;

			out.time(atTime);
			time_t timeDiff = after.time() - before.time();
			double interpolatePos = double(atTime - before.time()) / timeDiff;
			out.value((1 - interpolatePos) * before.value() + interpolatePos * after.value());
			return true;
		}

		return false;
	}

}
