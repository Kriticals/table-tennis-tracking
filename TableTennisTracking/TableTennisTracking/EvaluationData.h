#pragma once

#include <vector>
#include "MeasurementCollection.h"
#include <utMeasurement/Measurement.h>
class EvaluationData
{
public:
	TTT::MeasurementCollection<Ubitrack::Math::Vector3d> referenceData_;
	TTT::MeasurementCollection<Ubitrack::Math::Vector3d> trackingData_;
	TTT::MeasurementCollection<double> reprojectionErrors;
	TTT::MeasurementCollection<double> absoluteErrors;
	TTT::MeasurementCollection<Ubitrack::Math::Vector3d> errors;
	std::vector<double> reprojectionErrors_;
	std::vector<double> absoluteErrors_;
	std::vector<Ubitrack::Math::Vector3d> errors_;

	EvaluationData();
	~EvaluationData();
	void addTrackedData(TTT::Measurement<Ubitrack::Math::Vector3d> position3d, double reprojectionError, double absoluteError, Ubitrack::Math::Vector3d error);
};

