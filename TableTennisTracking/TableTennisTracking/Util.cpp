#include "Util.h"

namespace TTT {

	using namespace std;

	float Util::nanosecondsToSeconds(time_t t)
	{
		return t / 1000000000.0f;
	}

	time_t Util::secondsToNanoseconds(float t)
	{
		return (time_t)(t * 1000000000);
	}

	time_t Util::milliToNanoseconds(time_t milli)
	{
		return milli * 1000000;
	}

	float Util::clip(float n, float lower, float upper)
	{
		return std::max(lower, std::min(n, upper));
	}

	Ubitrack::Math::Vector2d Util::convertToUbitrackCoordinates(cv::Vec2f p1, int imageRows)
	{
		return Ubitrack::Math::Vector2d(p1[0], imageRows - 1 - p1[1]);
	}

	double Util::calculateReprojectionError(Ubitrack::Math::Matrix3x3d intrinsics, Ubitrack::Math::Vector3d estimate, Ubitrack::Math::Vector2d p_original, Ubitrack::Math::Vector2d &p_projected_out) {
		p_projected_out = project3Dto2D(intrinsics, Ubitrack::Math::Pose(), estimate);
		return boost::numeric::ublas::norm_2(p_original - p_projected_out);
	}

	Ubitrack::Measurement::CameraIntrinsics Util::loadCameraIntrinsics(const std::string file) {
		Ubitrack::Measurement::CameraIntrinsics K = Ubitrack::Measurement::CameraIntrinsics(Ubitrack::Math::CameraIntrinsics<double>());
		Ubitrack::Util::readCalibFile(file, K);
		return K;
	}

	Ubitrack::Measurement::Matrix3x3 Util::loadMatrix(const std::string file) {
		Ubitrack::Measurement::Matrix3x3 K = Ubitrack::Measurement::Matrix3x3(Ubitrack::Math::Matrix3x3d());
		Ubitrack::Util::readCalibFile(file, K);
		return K;
	}

	Ubitrack::Measurement::Pose Util::loadCameraExtrinsics(const std::string file) {
		Ubitrack::Measurement::Pose K = Ubitrack::Measurement::Pose(Ubitrack::Math::Pose());
		Ubitrack::Util::readCalibFile(file, K);
		return K;
	}

	MeasurementCollection<Ubitrack::Math::Vector3d> Util::loadReferencePositionData(const std::string fil, const std::string ts_file, time_t offset_ns)
	{
		MeasurementCollection<Ubitrack::Math::Vector3d> collection;
		std::ifstream file(fil);
		if (!file.good())
			UBITRACK_THROW("Could not open file " + fil);

		std::ifstream artfile(ts_file);
		if (!artfile.good())
			UBITRACK_THROW("Could not open file " + ts_file);

		boost::archive::text_iarchive archive(file);
		boost::archive::text_iarchive archive_art(artfile);

		// read contents until end-of-file exception
		try
		{
			while (true)
			{
				Ubitrack::Measurement::Position e(boost::shared_ptr< typename Ubitrack::Measurement::Position::value_type >(new typename Ubitrack::Measurement::Position::value_type()));
				Ubitrack::Measurement::Position e_art(boost::shared_ptr< typename Ubitrack::Measurement::Position::value_type >(new typename Ubitrack::Measurement::Position::value_type()));
				std::string dummy; // for newline character in archive
				archive >> dummy >> e;
				archive_art >> dummy >> e_art;
				time_t t = e_art.time() + offset_ns;
				Measurement<Ubitrack::Math::Vector3d> m(t, *e.get());
				collection.addMeasurement(m);
			}
		}
		catch (...)
		{
		}
		return collection;
	}

	Ubitrack::Math::Vector3d Util::compute3DPosition(Ubitrack::Math::Matrix3x3d intrinsics_cam1, Ubitrack::Math::Vector2d p_cam1, Ubitrack::Math::Matrix3x3d intrinsics_cam2, Ubitrack::Math::Pose extrinsics_cam2, Ubitrack::Math::Vector2d p_cam2) {
		Ubitrack::Math::Pose extrinsics_cam1 = Ubitrack::Math::Pose();
		Ubitrack::Math::Matrix< double, 3, 4 > P1(extrinsics_cam1);
		P1 = boost::numeric::ublas::prod(intrinsics_cam1, P1);

		Ubitrack::Math::Matrix< double, 3, 4 > P2(extrinsics_cam2);
		P2 = boost::numeric::ublas::prod(intrinsics_cam2, P2);

		Ubitrack::Math::Vector3d result = Ubitrack::Algorithm::get3DPosition(P1, P2, p_cam1, p_cam2);
		std::vector<Ubitrack::Math::Matrix3x4d> P;
		P.push_back(P1);
		P.push_back(P2);
		std::vector<Ubitrack::Math::Vector2d> points;
		points.push_back(p_cam1);
		points.push_back(p_cam2);
		double residual = -1;
		Ubitrack::Math::Vector3d result2 = Ubitrack::Algorithm::get3DPositionWithResidual(P, points, 1, &residual);
		return result;
	}

	Ubitrack::Math::Vector2d Util::project3Dto2D(Ubitrack::Math::Matrix3x3d intrinsics, Ubitrack::Math::Pose extrinsics, Ubitrack::Math::Vector3d X)
	{
		//Ubitrack::Math::Matrix< double, 3, 4 > projMatrix(extrinsics);
		Ubitrack::Math::Vector3d xProj = boost::numeric::ublas::prod(intrinsics, X);
		xProj /= xProj[2];
		//p /= double(p(2));
		//Ubitrack::Math::Vector< double, 3 > p = boost::numeric::ublas::prod(X, projMatrix);
		return Ubitrack::Math::Vector2d(xProj[0], xProj[1]);
	}

	double Util::round(double x, int n) {
		int d = 0;
		if ((x * pow(10, n + 1)) - (floor(x * pow(10, n))) > 4) d = 1;
		x = (floor(x * pow(10, n)) + d) / pow(10, n);
		return x;
	}
	float Util::convertSpeedMpsToKmh(float metersPerSecond)
	{
		return metersPerSecond * 3.6f;
	}
}