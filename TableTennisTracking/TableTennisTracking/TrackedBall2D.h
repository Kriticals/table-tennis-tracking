#pragma once

#include <vector>
#include "ContourProcessing.h"
#include "ContourPair.h"

namespace TTT{

	enum ChangeType {
		SURFACE_IMPACT,
		HORIZONTAL
	};

	struct DirectionChange {
		ChangeType type;
		int detectionIndex;
	};

	class TrackedBall2D
	{
		// Static counter to get unique ids
		static unsigned int s_idCounter;
		// The positions/detections of this ball
		std::vector<std::shared_ptr<ContourProperties>> detections_;
		// The pairs of tracked contours 
		std::vector<std::shared_ptr<ContourPair>> trackingPairs_;
		// Number of frames since last successful detection
		int missedFrames_;

		// TODO: Remove when done with evaluation of thesis
		std::vector<cv::Vec2f> predictions_;
		std::vector<bool> predictionsSuccess_;
		

	public:
		// Unique ball identifier
		const unsigned int Id;

		TrackedBall2D();
		~TrackedBall2D();
		void addDetection(std::shared_ptr<ContourProperties> detection);
		void addPair(std::shared_ptr<ContourPair> pair);
		void addMissedFrame();

		// Getters, Setters
		std::vector<std::shared_ptr<ContourProperties>>& detections();
		std::vector<std::shared_ptr<ContourPair>>& trackingPairs();
		std::vector<cv::Vec2f>& predictions();
		std::vector<bool>& predictionsSuccess();
		std::shared_ptr<ContourProperties> getLastDetection();
		int missedFrames();
		// Predict next location - split the timestep into numSteps for higher accuracy
		cv::Vec2f predictNextLocation(float timestep, int numSteps);
	};

}