#include "DrawingUtilities.h"
#include "ContourProperties.h"

namespace TTT {

	using namespace std;
	using namespace cv;

	void drawTrackedBall(cv::UMat & inout, shared_ptr<TrackedBall2D> ball, TrackedBallDrawMode drawMode, Scalar ballColor, Scalar lineColor, Scalar vertexColor)
	{
		switch (drawMode) {

		case CONTOUR:
		{
			for (int i = 0; i < ball->detections().size() - 1; ++i) {
				shared_ptr<ContourProperties> p = ball->detections()[i];
				drawSingleContour(inout, p->contour, lineColor);
			}

			if (ball->predictions().size() != ball->predictionsSuccess().size())
				cout << "Prediction size differen\n";

			/* For thesis only
			for (int i = 0; i < ball->predictions().size() && i < ball->predictionsSuccess().size(); ++i) {
				Scalar color = ball->predictionsSuccess()[i] ? Scalar(0, 255, 0) : Scalar(0, 0, 255);
				circle(inout, Point(ball->predictions()[i]), 3, color, -1);
			}*/

			drawSingleContour(inout, ball->detections().back()->contour, ballColor);
		}
		break;

		case LINE: 
		case LINE_VERTICES:
		{
			shared_ptr<ContourProperties> p_last;
			shared_ptr<ContourProperties> p = ball->detections().front();

			for (int i = 1; i < ball->detections().size(); ++i) {
				p_last = p;
				p = ball->detections()[i];
				line(inout, p_last->massCenter, p->massCenter, lineColor);
				if (drawMode == LINE_VERTICES)
					circle(inout, p->massCenter, 2, vertexColor, -1);
			}

			drawSingleContour(inout, ball->detections().back()->contour, ballColor);

		}
			break;

		}

		circle(inout, Point(ball->predictNextLocation(Configuration::instance().timeBetweenFrames, 4)), 3, Scalar(0, 0, 255));
		
	}


	void drawSingleContour(cv::UMat &inout, const vector<Point> &contour, Scalar &lineColor) {
		Point p1;
		Point p2 = contour[0];
		for (int i = 1; i < contour.size(); ++i) {
			p1 = p2;
			p2 = contour[i];
			line(inout, p1, p2, lineColor);
		}
		line(inout, contour[0], p2, lineColor);


}

	void drawRotatedRect(cv::UMat & inout, const cv::RotatedRect rRect, cv::Scalar color)
	{
		Point2f vertices[4];
		rRect.points(vertices);
		for (int i = 0; i < 4; i++)
			line(inout, vertices[i], vertices[(i + 1) % 4], Scalar(0, 255, 0), 2);
	}

}