#include "ContourProcessing.h"

namespace TTT {

	using namespace std;

	ContourCheckResult contourValidityCheck(shared_ptr<ContourProperties> properties)
	{
		float areaRatio = abs(properties->area / (properties->areaModelAprox) - 1);
		if (areaRatio > Configuration::instance().maxAreaRatioDeviation)
			return AREA_MODEL_DEVIATION;

		if (properties->area < Configuration::instance().minContourArea)
			return AREA_TOO_SMALL;

		if (properties->area > Configuration::instance().maxContourArea)
			return AREA_TOO_BIG;

		//if (properties->length / properties->diameter < 1.5f)
		//	return SPEED_LOW;

		return OK;
	}

}