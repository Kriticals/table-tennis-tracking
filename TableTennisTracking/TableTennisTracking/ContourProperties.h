#pragma once

#define _USE_MATH_DEFINES

#include <opencv2/imgproc.hpp>
#include <memory>
#include <stdio.h>
#include <iostream>
#include "Configuration.h"
#include "VectorOperations.h"
#include <math.h>

namespace TTT {

	enum OrientationApproximator
	{
		MIN_AREA_RECT,
		FIT_LINE,
		PCA_
	};

	class ContourProperties {

	public:
		const int frameId;
		const time_t frameTime;
		const std::vector<cv::Point> contour;
		mutable cv::Point2f massCenter;
		mutable cv::Point2f direction;
		mutable float diameter;
		mutable float radius;
		mutable float length;
		mutable float speedPixelsPerFrame;
		mutable float area;
		mutable float areaModelAprox;
		mutable cv::RotatedRect rotatedRect;
		bool assignedToBall;

		cv::Scalar colorAvgHSV;
		cv::Scalar colorMinHSV;
		cv::Scalar colorMaxHSV;
		std::shared_ptr<ContourProperties> last;
		std::shared_ptr<ContourProperties> next;
		ContourProperties(std::vector<cv::Point> contour, int frameId, time_t frameTime, OrientationApproximator orientationApproximator = MIN_AREA_RECT);
		~ContourProperties();

		void print();
	};

}

