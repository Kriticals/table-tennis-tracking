#pragma once

#include <opencv2/core.hpp>
#include "DetectorResult.h"
#include "ContourProperties.h"

namespace TTT {

	ContourCheckResult contourValidityCheck(std::shared_ptr<ContourProperties> properties);

}

