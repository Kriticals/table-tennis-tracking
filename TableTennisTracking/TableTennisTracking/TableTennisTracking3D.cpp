#include "TableTennisTracking3D.h"

namespace TTT {

	using namespace std;
	using namespace cv;

	TableTennisTracking3D::TableTennisTracking3D(string recordingBaseFolder = "", bool referenceDataAvailable) :
		numCameras_(2),
		trackingMode_(OFFLINE),
		recordingBaseFolder_(recordingBaseFolder),
		referenceDataAvailable_(referenceDataAvailable),
		frames(vector<UMat>(numCameras_)),
		maxReprojectionError_(20),
		intrinsics_cam1_(Util::loadCameraIntrinsics(recordingBaseFolder + "/Chamelion_16368838_OpenCV.txt")),
		intrinsics_cam2_(Util::loadCameraIntrinsics(recordingBaseFolder + "/Chamelion_16369295_OpenCV.txt")),
		extrinsics_cam2(Util::loadCameraExtrinsics(recordingBaseFolder + "/left_to_right_pose_stereo.txt")),
		intrinsics_cam1_undist_(Util::loadMatrix(recordingBaseFolder + "/intrinsics_left.txt")),
		intrinsics_cam2_undist_(Util::loadMatrix(recordingBaseFolder + "/intrinsics_right.txt"))
	{
		pendingBalls.resize(numCameras_);

		if (referenceDataAvailable_) {
			// Adjust reference data due to difference in exposure time. Ref. data is recorded with a very fast shutter speed (prob. less than 1/300s)
			time_t offset = 0; //(time_t)(Configuration::instance().exposure / 2.0 * 1000000000);
			evaluations.referenceData_ = Util::loadReferencePositionData(recordingBaseFolder_ + "/BallPosition3d_CameraLeft.log", recordingBaseFolder_ + "/BallPosition3d_ART.log", offset);
		}

		// Initialize 2D trackers
		for (size_t cameraId = 0; cameraId < numCameras_; ++cameraId) {
			trackers_.push_back(TableTennisTracking2D(cameraId));
			trackers_[cameraId].addObserver(this);
			videoCaptures_.push_back(VideoCapture(recordingBaseFolder_ + "/" + (cameraId == 0 ? "left" : "right") + "/img00000.png"));
			recordingFiles_.push_back(ifstream());
		}

		switch (trackingMode_) {
		case OFFLINE:
			processRecording();
			break;

		case LIVE:
			processCameraFeed();
			break;
		}
		
	}


	TableTennisTracking3D::~TableTennisTracking3D()
	{
	}

	bool TableTennisTracking3D::startUbitrackDataflow()
	{
		boost::shared_ptr< Ubitrack::Facade::AdvancedFacade > utFacade;
		try {
			utFacade.reset(new Ubitrack::Facade::AdvancedFacade(true, "C:/ubitrack/install/bin/ubitrack/"));
			utFacade->loadDataflow("C:/ubitrack/LiveImages.dfg");
		}
		catch (Ubitrack::Util::Exception e) {
			std::cout << e;
			return false;
		}

		leftCameraSink = utFacade->componentByName<Ubitrack::Components::ApplicationPushSink<Ubitrack::Measurement::ImageMeasurement> >("imageLeft");
		//rightCameraSink = utFacade->componentByName<Ubitrack::Components::ApplicationPushSink<Ubitrack::Measurement::ImageMeasurement> >("imageRight");

		leftCameraSink->setCallback(&onCameraImageLeft);
		//rightCameraSink->setCallback(&TableTennisTracking3D::onCameraImageRight);

		//boost::shared_ptr<Ubitrack::Components::ApplicationPushSource<Ubitrack::Measurement::ImageMeasurement> > imageComp = utFacade->componentByName<Ubitrack::Components::ApplicationPushSource<Ubitrack::Measurement::ImageMeasurement> >("image");
		//boost::shared_ptr<Ubitrack::Components::ApplicationPushSource<Ubitrack::Measurement::Position> > positionComp = utFacade->componentByName<Ubitrack::Components::ApplicationPushSource<Ubitrack::Measurement::Position> >("ballPosition");
		
		utFacade->startDataflow();

		//imageComp = utFacade->componentByName<Ubitrack::Components::ApplicationPushSource<Ubitrack::Measurement::ImageMeasurement> >("image");
		//positionComp = utFacade->componentByName<Ubitrack::Components::ApplicationPushSource<Ubitrack::Measurement::Position> >("ballPosition");

		return true;
	}

	void TableTennisTracking3D::processRecording()
	{

		for (size_t i = 0; i < numCameras_; ++i) {
			recordingFiles_[i].open(recordingBaseFolder_ + "/" + (i == 0 ? "left" : "right") + "/frames.log");
		}

		bool run = true;
		size_t counter = 0;
		while (run) {
			counter++;
			//cout << "Frame count: " << counter << endl;
			// Read frames
			time_t timestamp[2];
			string filename[2];
			for (size_t i = 0; i < numCameras_; ++i) {
				// Read timestamps of image files
				recordingFiles_[i] >> timestamp[i];
				recordingFiles_[i] >> filename[i]; // irrelevant right now

				if (!videoCaptures_[i].read(frames[i])) {
					run = false;
					break;
				}
			}

			if (!run)
				break;

			// Synchronize frames if necessary
			time_t minStamp;
			time_t maxStamp;
			Util::minMax<time_t>(timestamp, numCameras_, minStamp, maxStamp);
			while(maxStamp - minStamp > Util::milliToNanoseconds(2)) {
				for (size_t i = 0; i < numCameras_; ++i) {
					if (maxStamp - timestamp[i] > Util::milliToNanoseconds(2)) {
						recordingFiles_[i] >> timestamp[i];
						recordingFiles_[i] >> filename[i];
						videoCaptures_[i].read(frames[i]);
						Util::minMax<time_t>(timestamp, numCameras_, minStamp, maxStamp);
						break;
					}
				}
			}

			// Process frames
			for (size_t i = 0; i < numCameras_; i++) {
				currentTimestamp_ = timestamp[i];
				trackers_[i].handleFrame(frames[i], timestamp[i]);
			}
			afterFrameProcessing();

			FrameData frameData;
			trackers_[1].getFrameData(frameData);
			imwrite("C:/ubitrack/outputimages/forvideo" + to_string(counter) + ".png", frameData.output);
			
			bool waitForInput = false;
			char keyboardInput = waitKey(1);
			do {
				keyboardInput = waitKey(10);
				if (keyboardInput == 't') {
					cout << "\nTimestamp: " << timestamp[0] << "\n";
				}
				if (keyboardInput == 's') {
					FrameData frameData;
					trackers_[1].getFrameData(frameData);
					imwrite("C:/ubitrack/outputimages/output.png", frameData.output);
					imwrite("C:/ubitrack/outputimages/fgmask.png", frameData.foregroundMask);
					imwrite("C:/ubitrack/outputimages/fgmaskuo.png", frameData.foregroundMaskUnopened);
				}
			} while (true && keyboardInput != 'n');
		}

	}

	// TODO
	void TableTennisTracking3D::processCameraFeed()
	{
		if (startUbitrackDataflow()) {
			cout << "Ubitrack dataflow started." << endl;
		}
		else {
			cout << "Couldn't start Ubitrack dataflow." << endl;
		}

		char keyboardInput = waitKey(1);
		do {
			keyboardInput = waitKey(10);
		} while (keyboardInput != 'q');
	}

	void TableTennisTracking3D::afterFrameProcessing()
	{
		// Remove any tracked balls whose 2D trackers are not synchronized anymore, i.e. one tracker failed to continue tracking.
		// Have to because: If one of the two 2d trackers successfully continues to track, it has to be matched with a new corresponding tracked 2d ball.
		std::vector<shared_ptr<TrackedBall3D>> toRemove;
		for (auto kvp : trackedBalls_) {
			auto trackedBall = kvp.second;
			if (!trackedBall->pending() && !trackedBall->synchronized()) {
				toRemove.push_back(trackedBall);
			}
		}
		for (auto outOfSyncBall : toRemove) {
			for (size_t i = 0; i < numCameras_; ++i) {
				if(outOfSyncBall->hasCamera(i))
					trackedBalls_.erase(outOfSyncBall->getBall2d(i)->Id);
			}
			
			trackedBalls_.erase(outOfSyncBall->getBall2d(1)->Id);
		}
	}

	bool TableTennisTracking3D::tryCompute3Destimate(std::shared_ptr<TrackedBall3D> ball, Ubitrack::Math::Vector3d & out, double &reprojectionError)
	{
		if (!ball->hasCamera(0) || !ball->hasCamera(1))
			return false;

		auto ball_camera0 = ball->getBall2d(0);
		auto ball_camera1 = ball->getBall2d(1);

		auto lastDetection_camera0 = ball_camera0->getLastDetection();
		auto lastDetection_camera1 = ball_camera1->getLastDetection();
		// Check if synchronized and allow max. 2ms offset
		if (abs(lastDetection_camera0->frameTime - lastDetection_camera1->frameTime) > 2000000)
			return false;

		// Estimate 3d position
		Ubitrack::Math::Vector2d p0 = Util::convertToUbitrackCoordinates(lastDetection_camera0->massCenter, frames[0].rows);
		Ubitrack::Math::Vector2d p1 = Util::convertToUbitrackCoordinates(lastDetection_camera1->massCenter, frames[1].rows);
		out = Util::compute3DPosition(*intrinsics_cam1_undist_, p0, *intrinsics_cam2_undist_, ~(*extrinsics_cam2.get()), p1);

		// Compute reprojection error
		Ubitrack::Math::Vector2d proj;
		reprojectionError = Util::calculateReprojectionError(*intrinsics_cam1_undist_, out, p0, proj);
		return true;
	}

	bool TableTennisTracking3D::tryMatchBall(std::shared_ptr<TrackedBall2D> trackedBall, size_t cameraId)
	{
		size_t otherCameraId = (cameraId + 1) % 2;
		shared_ptr<TrackedBall3D> pendingBall = make_shared<TrackedBall3D>(numCameras_);
		pendingBall->setBall2d(cameraId, trackedBall);

		for (auto it = pendingBalls[otherCameraId].begin(); it != pendingBalls[otherCameraId].end(); ) {
			shared_ptr<TrackedBall2D> otherBall = *it;
			if (otherBall->missedFrames() > 1) {
				it = pendingBalls[otherCameraId].erase(it);
				continue;
			}

			pendingBall->setBall2d(otherCameraId, otherBall);
			// Assume match and calculate reprojection error
			Ubitrack::Math::Vector3d estimate3d;
			double reprojectionError;
			if (tryCompute3Destimate(pendingBall, estimate3d, reprojectionError) && reprojectionError < maxReprojectionError_) {
				// Found matching pair
				trackedBalls_[trackedBall->Id] = pendingBall;
				it = pendingBalls[otherCameraId].erase(it);
				inPendingBalls_.erase(otherBall->Id);
				return true;
			}
			else {
				// Reprojection error too high
				pendingBall->removeBall2d(otherCameraId);
			}

			it++;
		}

		return false;
	}

	void TableTennisTracking3D::onBallBounce(const std::shared_ptr<TrackedBall2D> ball, Vec2f bounceLocation, TableTennisTracking2D * tracker)
	{
		// TODO: Match impact points from both trackers to obtain bounce location in 3D
	}

	void TableTennisTracking3D::onBallTracked(const std::shared_ptr<TrackedBall2D> trackedBall, TableTennisTracking2D* tracker)
	{
		// Check if this ball is already matched with another tracker's 2d ball 
		auto it = trackedBalls_.find(trackedBall->Id);
		if (it != trackedBalls_.end()) {
			shared_ptr<TrackedBall3D> ball3d = it->second;
			// Don't continue if ball data is not up to date, i.e. 2d tracking data is missing
			if (!ball3d->synchronized())
				return;

			Ubitrack::Math::Vector3d estimate3d;
			Ubitrack::Math::Vector2d pProj;

			double reprojectionError;
			if (tryCompute3Destimate(ball3d, estimate3d, reprojectionError) && reprojectionError < maxReprojectionError_) {

				// We have new 3d data! Save to balls trajectory
				time_t frameTime = ball3d->getBall2d(0)->getLastDetection()->frameTime;
				ball3d->addToTrajectory({frameTime, estimate3d});
				float speedMps = ball3d->speed();
				if (speedMps >= 0) {
					float speedKmh = Util::convertSpeedMpsToKmh(speedMps);
					std::ostringstream out;
					out.precision(2);
					out << speedKmh;
					
					tracker->addText(trackedBall->getLastDetection()->massCenter + Point2f(-20, 50), out.str() + " km/h");
				}
				
				cout << estimate3d << endl;

				// Add evaluation data if reference data is available
				if (referenceDataAvailable_) {
					Measurement<Ubitrack::Math::Vector3d> ref;
					if (evaluations.referenceData_.TryGetMeasurement(frameTime, (time_t)50 * 1000000, ref)) {
						

						//Calculate error
						Ubitrack::Math::Vector3d error = estimate3d - ref.value();
						double absoluteError = boost::numeric::ublas::norm_2(error);
						cout << "Abs. Error: " << absoluteError << endl;
						cout << "Rep. Error: " << reprojectionError << endl;

						// Reproject ART
						Ubitrack::Math::Vector2d p1 = Util::convertToUbitrackCoordinates(ball3d->getBall2d(0)->getLastDetection()->massCenter, frames[0].rows);
						Ubitrack::Math::Vector2d pProj_art;
						Util::calculateReprojectionError(*intrinsics_cam1_undist_, ref.value(), p1, pProj_art);

						//Reproject Estimate
						Ubitrack::Math::Vector2d pProj;
						Util::calculateReprojectionError(*intrinsics_cam1_undist_, estimate3d, p1, pProj);

						// Show reprojections
						cv::Mat tmp;
						frames[0].copyTo(tmp);
						pProj_art = Util::convertToUbitrackCoordinates(Vec2f(pProj_art[0], pProj_art[1]), tmp.rows);
						pProj = Util::convertToUbitrackCoordinates(Vec2f(pProj[0], pProj[1]), tmp.rows);
						cv::circle(tmp, cv::Point(pProj_art[0], pProj_art[1]), 3, Scalar(0, 255, 0));
						cv::circle(tmp, cv::Point(pProj[0], pProj[1]), 3, Scalar(0, 0, 255));
						imshow("Reprojections(ART: Green, Estimate: Red)", tmp);
						imwrite("C:/ubitrack/outputimages/forvideoReproj" + to_string(counterrr++) + ".png", tmp);

						// Had to manually filter 3 or 4 estimates, since they were not from the ball
						bool save = true;
						/*if (absoluteError > 0.15) {
							cout << "High error: accept value in output? i = ignore, a = accept";
							char in = waitKey(1);
							while (in != 'a') {
								in = waitKey(20);
								if (in == 'i') {
									save = false;
									break;
								}
							}
						}*/
						if(save)
							evaluations.addTrackedData({ trackedBall->getLastDetection()->frameTime, estimate3d }, reprojectionError, absoluteError, error);

					}
				}
				
			}
		}
		else {
			// Try to match ball, and if it doesn't work, add it to pending balls to be matched at a later point in time
			if (!tryMatchBall(trackedBall, tracker->cameraId_)) {
				auto inPending = inPendingBalls_.find(trackedBall->Id);
				if (inPending == inPendingBalls_.end()) {
					pendingBalls[tracker->cameraId_].push_back(trackedBall);
					inPendingBalls_[trackedBall->Id] = true;
				}
			}
		}
	}

	void onCameraImageLeft(Ubitrack::Measurement::ImageMeasurement image)
	{
		imshow("point grey left", image.get()->uMat());
	}

	void onCameraImageRight(Ubitrack::Measurement::ImageMeasurement image)
	{
		imshow("point grey right", image.get()->uMat());
	}

}