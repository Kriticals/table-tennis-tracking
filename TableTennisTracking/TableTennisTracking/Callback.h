#pragma once

namespace TTT {

	template<typename... Types>
	class Callback {

	public:
		virtual ~Callback() {}
		virtual void invoke(Types ...) = 0;
		
	};

	template<typename C, typename... Types>
	class MemberCallback : Callback<Types...> {

		typedef void (C::* MemberFunctionPointer) (Types...);

		C* object_;
		MemberFunctionPointer functionPointer_;

	public:
		MemberCallback(C* object, MemberFunctionPointer memberFunctionPointer) :
			functionPointer_(memberFunctionPointer)
		{	}

		virtual void invoke(Types... args) {
			object_->*functionPointer_(args...);
		}

	};

}