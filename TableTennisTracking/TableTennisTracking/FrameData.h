#pragma once


#include <opencv2/core/mat.hpp>
#include "ContourProperties.h"

namespace TTT {

	struct FrameData {

		size_t id;

		time_t time;
		
		cv::UMat input;

		cv::UMat foregroundMask;

		cv::UMat foregroundMaskUnopened;

		cv::UMat foregroundMaskColor;

		cv::UMat output;

		std::vector<std::vector<cv::Point>> contours;

		std::vector<std::vector<cv::Point>> candidateContours;

		std::vector<std::shared_ptr<ContourProperties>> candidates;

	};

}

