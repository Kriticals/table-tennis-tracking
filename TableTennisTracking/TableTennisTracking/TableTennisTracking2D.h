#pragma once

#define _USE_MATH_DEFINES

#include <stdio.h>
#include <iostream>
#include <vector>
#include <list>
#include <deque>
#include <opencv2\imgproc.hpp>
#include <opencv2\highgui.hpp>
#include <opencv2\video\background_segm.hpp>
#include <math.h>
#include "DrawingUtilities.h"
#include "ContourPair.h"
#include "ContourProperties.h"
#include "DetectorResult.h"
#include "TrackedBall2D.h"
#include "Configuration.h"
#include "ContourPairCheck.h"
#include "FrameData.h"
#include "Callback.h"
#include <utMeasurement/Measurement.h>
#include "Util.h"

namespace TTT {

	enum TTTState {
		BACKGROUND_INITIALIZATION,
		TABLE_INITIALIZATION,
		BALL_INITIALIZATION,
		DETECTION,
		DETECTION_VERIFICATION,
		TRACKING
	};

	class TableTennisTracking2D;

	class TrackingObserver2D
	{
	public:
		virtual ~TrackingObserver2D() {}
		virtual void onBallTracked(const std::shared_ptr<TrackedBall2D> ball, TableTennisTracking2D* tracker) = 0;
		virtual void onBallBounce(const std::shared_ptr<TrackedBall2D> ball, cv::Vec2f bounceLocation, TableTennisTracking2D* tracker) = 0;
	};

	class TableTennisTracking2D {
		

		char keyboard_;
		int selectedContour1_ = -1;
		int selectedContour2_ = -1;

		Configuration &conf_;
		// Current state of the tracking
		TTTState state_;
		// Number of frames received
		size_t frameCounter_;
		// Whether or not the background model will be updated
		bool updateBackground_;
		cv::Ptr<cv::BackgroundSubtractorMOG2> backgroundSubtractor_;
		// Checks that need to run successfully to detect two contours as a new ball
		std::vector<std::shared_ptr<ContourPairCheck>> detectionChecks_;
		// Checks that need to run successfully to add a contour to a tracked ball
		std::vector<std::shared_ptr<ContourPairCheck>> trackingChecks_;
		// History of Frame-data, i.e. contours, filtered contours, input and output images etc.
		std::deque<std::shared_ptr<FrameData>> frameDataList_;
		// Currently tracked balls
		std::list<std::shared_ptr<TrackedBall2D>> trackedBalls_;
		// Method used to approximate a contours' orientation
		OrientationApproximator orientationApproximator_;
		// Observers of tracking events
		std::vector<TrackingObserver2D*> observers_;
		// Size of opening operation
		int openSize_ = 1;
		// Kernel for performing opening
		cv::Mat kernelOpen_;

	public:
		const size_t cameraId_;

		TableTennisTracking2D(size_t cameraId);
		void changeState(TTTState newState);
		void handleFrame(cv::UMat input, time_t time = 0);
		// Generates foreground mask
		void preprocessing(std::shared_ptr<FrameData> frameData);
		// Find contours that could be a ball in foreground mask
		bool findBallCandidates(std::shared_ptr<FrameData> frameData);
		// Detection of balls based on two frames
		void detectBalls(std::shared_ptr<FrameData> lastFrame, std::shared_ptr<FrameData> currentFrame);
		// Tracking by numerical integration
		void trackBalls(std::shared_ptr<FrameData> currentFrame);

		void drawCandidates(cv::UMat & input, FrameData & result, cv::Scalar color);
		ContourCheckResult performContourChecks(const std::shared_ptr<ContourPair> pair, const std::vector<std::shared_ptr<ContourPairCheck>> &checks);
		void onMouseInternal(int event, int x, int y, int);
		int findDetectionAtPosition(int x, int y, std::shared_ptr<FrameData> frameData);
		// Converts ENUM to string for debugging
		std::string DetectionCheckResult2String(ContourCheckResult r);
		bool getFrameData(FrameData & result);
		std::list<std::shared_ptr<TrackedBall2D>>& trackedBalls();
		void addObserver(TrackingObserver2D *observer);
		void drawOutput(bool val);
		void addText(cv::Point position, std::string text, cv::Scalar color = cv::Scalar(255, 255, 255));
		void drawCircle(cv::Point position, cv::Scalar color);
	};

	static void onMouseDetection(int event, int x, int y, int, void*);

}

