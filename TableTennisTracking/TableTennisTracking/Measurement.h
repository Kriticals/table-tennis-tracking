#pragma once

namespace TTT {

	template <typename T>
	class Measurement {
		time_t time_;
		T value_;
	public:
		Measurement() :
			time_(-1),
			value_(T{})
		{}

		Measurement(time_t time, T value);

		bool operator<(const Measurement& m) const;

		operator T &();

		T& value();

		void value(T v);

		time_t time();

		void time(time_t t);
	};

	template<typename T>
	inline Measurement<T>::Measurement(time_t time, T value) :
		time_(time),
		value_(value)
	{}

	template<typename T>
	inline bool Measurement<T>::operator<(const Measurement & m) const
	{
		return (this->time_ < m.time_);
	}

	template<typename T>
	inline Measurement<T>::operator T&()
	{
		return value();
	}

	template<typename T>
	inline T & Measurement<T>::value()
	{
		return value_;
	}

	template<typename T>
	inline void Measurement<T>::value(T v)
	{
		value_ = v;
	}

	template<typename T>
	inline time_t Measurement<T>::time()
	{
		return time_;
	}

	template<typename T>
	inline void Measurement<T>::time(time_t t)
	{
		time_ = t;
	}

}
