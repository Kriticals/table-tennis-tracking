#pragma once

#define _USE_MATH_DEFINES

#include <vector>
#include <opencv2/core/types.hpp>
#include <math.h>

namespace TTT {

	struct Projection {
		float min, max;

		float length() {
			return max - min;
		}
	};

	Projection project(const std::vector<cv::Point>& contour, cv::Vec2f projectionAxis);

	float projectionLength(const std::vector<cv::Point>& contour, cv::Vec2f projectionAxis);

	float angleBetweenVectors(cv::Vec2f v1, cv::Vec2f v2);

	cv::Vec2f normalVector(cv::Vec2f &v);

	bool vectorIntersection(cv::Vec2f v1, cv::Vec2f d1, cv::Vec2f v2, cv::Vec2f d2, cv::Vec2f &out);

	float vectorLength(cv::Vec2f v);
	float vectorLength(cv::Vec3f v) ;

}