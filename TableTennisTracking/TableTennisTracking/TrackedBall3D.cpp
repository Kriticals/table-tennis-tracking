#include "TrackedBall3D.h"

namespace TTT {

	using namespace std;

	TrackedBall3D::TrackedBall3D(size_t numCameras) : 
		ballsAvailable_(0)
	{
		balls2d_.resize(numCameras);
	}

	TrackedBall3D::~TrackedBall3D()
	{
	}

	void TrackedBall3D::setBall2d(int cameraId, std::shared_ptr<TrackedBall2D> ball2d)
	{
		if (balls2d_[cameraId] == nullptr) {
			balls2d_[cameraId] = ball2d;
			ballsAvailable_.push_back(cameraId);
		}
	}

	void TrackedBall3D::removeBall2d(int cameraId)
	{
		ballsAvailable_.remove(cameraId);
		balls2d_[cameraId] = nullptr;
	}

	bool TrackedBall3D::pending()
	{
		return ballsAvailable_.size() < balls2d_.size();
	}

	bool TrackedBall3D::synchronized()
	{
		if (pending())
			return false;

		if (balls2d_[0] != nullptr) {
			time_t time = balls2d_[0]->detections().back()->frameTime;
			for (size_t i = 1; i < balls2d_.size(); ++i) {
				std::shared_ptr<TrackedBall2D> ball2d = balls2d_[i];
				if (ball2d == nullptr || abs(time - ball2d->detections().back()->frameTime) > 2000000)
					return false;
			}
		}
		return true;
	}

	bool TrackedBall3D::hasCamera(size_t cameraId)
	{
		for (size_t camAvailable : ballsAvailable_)
			if (cameraId == camAvailable)
				return true; 

		return false;
	}

	std::shared_ptr<TrackedBall2D> TrackedBall3D::getBall2d(size_t idx)
	{
		if (balls2d_.size() > idx)
			return balls2d_[idx];
		else
			return nullptr;
	}

	void TrackedBall3D::addToTrajectory(Measurement<Ubitrack::Math::Vector3d> estimate3d)
	{
		trajectory_.addMeasurement(estimate3d);
	}

	MeasurementCollection<Ubitrack::Math::Vector3d>& TrackedBall3D::trajectory()
	{
		return trajectory_;
	}

	float TrackedBall3D::speed()
	{
		if (trajectory_.measurements_.size() > 1) {
			auto it = trajectory_.measurements_.rbegin();
			auto mea = (*it);
			time_t lastPosTime = mea.time();
			Ubitrack::Math::Vector3d lastPos = mea.value();
			int i = 1;
			while (i < 4 && i < trajectory_.measurements_.size()) {
				it++;
				i++;
			}
			mea = (*it);
			time_t prevLastPosTime = mea.time();
			Ubitrack::Math::Vector3d prevLastPos = mea.value();
			Ubitrack::Math::Vector3d diff = lastPos - prevLastPos;
			cv::Vec3d diffcv = cv::Vec3d(diff[0], diff[1], diff[2]);
			return abs(sqrt(diffcv.ddot(diffcv)) / ((double)(lastPosTime - prevLastPosTime) / 1000000000.0));
		}
		else {
			return -1;
		}
	}

}
