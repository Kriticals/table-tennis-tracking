//opencv
#include "opencv2/imgproc.hpp"
#include "opencv2/videoio.hpp"
#include <opencv2/highgui.hpp>
#include <opencv2/video.hpp>
#include <opencv2/core/ocl.hpp>
//C
#include <stdio.h>
#include "time.h"
//C++
#include <iostream>
//Boost
#include <boost/algorithm/string.hpp>
// Table Tennis Tracking
#include "TableTennisTracking3D.h"
#include "RecordingInfo.h"


using namespace TTT;
using namespace std;
using namespace cv;


char keyboard; //input from keyboard

int main(int argc, char * argv[]);

// Run single 2d tracker
void processVideoSingle(string file, double fps, double exposure);
// Run single 3d tracker from recording
void processRecording3D(RecordingInfo recordingInfo);
// Runs all recordings defined to calculate mean absolute errors and export all errors to CSV file and latex table
void evaluateMultipleRecordings(vector<RecordingInfo> &recordingInfos);
// Writes all evaluation data to CSV file
void writeCSV(std::ofstream& myfile, EvaluationData ev, double timeBetweenFrames);
// Write single line of data at timestamp to CSV file
void writeCSVLine(std::ofstream& myfile, EvaluationData ev, time_t timestamp, double timeBetweenFrames);


// Just writing files for ubitrack
void convertSerializedFiles();


int main(int argc, char* argv[])
{

	convertSerializedFiles();

	// Define recordings available with reference data
	vector<RecordingInfo> recordingInfosEvaluation = {
		RecordingInfo("C:/TableTennisTracking/Recordings/NewVideoPG", 9.998 / 1000.0, 60.043, 1567180749056367000, 1567180749672370000)
		/*RecordingInfo("C:/ApendixDVD/DatasetJPG/WithReference/60FPS_5SHUTTER_LIGHT_BOUNCE", 5.0 / 1000.0, 60.158, 1551634240843762000, 1551634241510113000), // 0
		RecordingInfo("C:/ApendixDVD/DatasetJPG/WithReference/60FPS_5SHUTTER_LIGHT_BOUNCE_FAIL", 5.0 / 1000.0, 60.158, 1551635031526245000, 1551635031776575000), // 1
		RecordingInfo("C:/ApendixDVD/DatasetJPG/WithReference/60FPS_10SHUTTER_LIGHT_BOUNCE", 10.0 / 1000.0, 60.158, 1551631818818145000, 1551631819501318000), // 2
		RecordingInfo("C:/ApendixDVD/DatasetJPG/WithReference/60FPS_15SHUTTER_LIGHT_OCCLUSION", 15.0 / 1000.0, 60.158, 1551635590325035000, 1551635594554999000), // 3
		RecordingInfo("C:/ApendixDVD/DatasetJPG/WithReference/60FPS_15SHUTTER_LIGHT_BOUNCE", 15.0 / 1000.0, 60.158, 1551634084014045000, 1551634084631377000), // 4
		RecordingInfo("C:/ApendixDVD/DatasetJPG/WithReference/60FPS_15SHUTTER_THROW", 15.0 / 1000.0, 60.158, 1551636293026095000, 1551636293627454000), // 5
		RecordingInfo("C:/ApendixDVD/DatasetJPG/WithReference/60FPS_15SHUTTER_THROW2", 15.0 / 1000.0, 60.158, 1551636410176797000, 1551636410893880000), // 6
		RecordingInfo("C:/ApendixDVD/DatasetJPG/WithReference/60FPS_15SHUTTER_THROW3", 15.0 / 1000.0, 60.158, 1551636647414250000, 1551636647948822000), // 7
		RecordingInfo("C:/ApendixDVD/DatasetJPG/WithReference/60FPS_15SHUTTER_THROW4", 15.0 / 1000.0, 60.158, 1550670288638018000, 1550670289004083000), // 8*/
	};
	// Uncomment to save evaluation data for multiple recordings defined in the function
	evaluateMultipleRecordings(recordingInfosEvaluation);

	// Run a single 3D tracker
	
	//RecordingInfo singleRecording = RecordingInfo("C:/Users/Joe/Data/60FPS_5SHUTTER_PLAYING", 5.0 / 1000.0, 60.158, 0, -1);
	RecordingInfo singleRecording = RecordingInfo("C:/TableTennisTracking/Recordings/NewVideoPG", 9.998 / 1000.0, 60.043, 1567180749056367000, 1567180749672370000);
	//processRecording3D(singleRecording);
	
	// Uncomment to run single 2D tracker
	/*double fps = 60;
	double exposure = 15.0 / 1000.0;
	processVideoSingle("C:/ApendixDVD/DatasetJPG/WithoutReference/60FPS_5SHUTTER_PLAYING/left/img00000.jpg", fps, exposure);*/
	exit(EXIT_SUCCESS);
}

void convertSerializedFiles() {
	double R_mat_vals[9] = { 
		9.9902599230056410e-01, -2.5269565434235065e-02, -3.6173412482623582e-02, 
		2.5265888762581436e-02,	9.9968061027882660e-01, -5.5883592384240962e-04, 
		3.6175980607441399e-02, -3.5566180259908643e-04, 9.9934537169677840e-01 
	};
	Ubitrack::Math::Matrix3x3d R_mat = Ubitrack::Math::Matrix3x3d(R_mat_vals);
	Ubitrack::Math::Quaternion R = Ubitrack::Math::Quaternion(R_mat);

	R = Ubitrack::Math::Quaternion(-R.x(), R.y(), R.z(), -R.w());

	double T_vals[3] = { -2.3269575346649228e+02, -2.7038419377189453e+0*-1.0, -1.5836851640586971e+01*-1.0 };
	Ubitrack::Math::Vector3d T = Ubitrack::Math::Vector3d(T_vals) / 1000;
	Ubitrack::Math::Pose p(R, T);
	p = ~p;
	cout << "R: " << R << endl;
	cout << "T:" << T << endl;
	cout << "Pose:" << p << endl;
}

void processVideoSingle(string file, double fps, double exposure) {
	TableTennisTracking2D tracker = TableTennisTracking2D(0);
	tracker.drawOutput(true);
	VideoCapture capture = VideoCapture(file);
	// Time between frames in nanoseconds
	double tbf = 1 / fps * 1000000000;
	double millis = 0;
	UMat frame;
	while (capture.read(frame)) {
		// Generate timestamp
		millis += tbf;
		tracker.handleFrame(frame, (time_t)(millis));

		// Press n to proceed to next frame
		keyboard = waitKey(1);
		while (keyboard != 'n') {
			keyboard = waitKey(10);
		}
	}
}

void processRecording3D(RecordingInfo recordingInfo) {
	Configuration::instance().fps = recordingInfo.fps;
	Configuration::instance().timeBetweenFrames = 1.0 / recordingInfo.fps;
	Configuration::instance().exposure = recordingInfo.exposure;
	TableTennisTracking3D tracker3d = TableTennisTracking3D(recordingInfo.baseFolder, true);
}

void evaluateMultipleRecordings(vector<RecordingInfo> &recordingInfos) {

	std::ofstream csvFile;
	csvFile.open("C:/tracking_data_with_errors_new.csv");
	csvFile << "Est;X-axis;Y-axis;Z-axis;AE;RE" << endl;

	std::ofstream csvFileFlight;
	csvFileFlight.open("C:/tracking_data_with_errors_flight_new.csv");
	csvFileFlight << "Est;X-axis;Y-axis;Z-axis;AE;RE" << endl;

	ofstream resultTableFile;
	resultTableFile.open("C:/result_error_table_new.tex");
	// Start Latex Table
	resultTableFile << 
		"\\begin{center}\n" <<
		"\\begin{tabular}{ l | l | l | l | l }\n" <<
		"\\hline\n" <<
		"\\textbf{Recording} & \\textbf{AE} & \\textbf{AEF} & \\textbf{RE} & \\textbf{REF} \\\\ \\hline\n";

	for each (auto recordingInfo in recordingInfos)
	{
		string baseFolder = recordingInfo.baseFolder;
		Configuration::instance().exposure = recordingInfo.exposure;
		Configuration::instance().fps = recordingInfo.fps;
		Configuration::instance().timeBetweenFrames = 1 / recordingInfo.fps;
		TableTennisTracking3D tracker = TableTennisTracking3D(baseFolder, true);
		EvaluationData e = tracker.evaluations;
		
		// Write Errors for tracking during flight/throw
		for each(Measurement<Ubitrack::Math::Vector3d> est in e.trackingData_.measurements_) {
			// Only write data if its inside the flight time
			if (est.time() >= recordingInfo.throwStartTime && (est.time() <= recordingInfo.throwEndTime || recordingInfo.throwEndTime < 0)) {
				writeCSVLine(csvFileFlight, e, est.time(), 1.0 / recordingInfo.fps);
			}
		}

		size_t flightErrorCount = 0;
		double flightErrorSum = 0;
		double errorSum = 0;
		for each(Measurement<double> error in e.absoluteErrors.measurements_) {
			errorSum += error.value();
			if (error.time() >= recordingInfo.throwStartTime && (error.time() <= recordingInfo.throwEndTime || recordingInfo.throwEndTime < 0)) {
				flightErrorSum += error.value();
				flightErrorCount++;
			}
		}
		flightErrorSum /= flightErrorCount;
		flightErrorSum *= 100.0;
		flightErrorSum = Util::round(flightErrorSum, 3);
		errorSum /= e.absoluteErrors.measurements_.size();
		errorSum *= 100.0;
		errorSum = Util::round(errorSum, 3);

		size_t reFlightErrorCount = 0;
		double reFlightErrorSum = 0;
		double reErrorSum = 0;
		for each(Measurement<double> error in e.reprojectionErrors.measurements_) {
			reErrorSum += error.value();
			if (error.time() >= recordingInfo.throwStartTime && (error.time() <= recordingInfo.throwEndTime || recordingInfo.throwEndTime < 0)) {
				reFlightErrorSum += error.value();
				reFlightErrorCount++;
			}
		}
		reFlightErrorSum /= reFlightErrorCount;
		reFlightErrorSum = Util::round(reFlightErrorSum, 3);
		reErrorSum /= e.reprojectionErrors.measurements_.size();
		reErrorSum = Util::round(reErrorSum, 3);

		std::vector<std::string> results;
		boost::split(results, baseFolder, [](char c) {return c == '/'; });
		std::string recordingName = results.back();
		std::replace(recordingName.begin(), recordingName.end(), '_', ' ');

		resultTableFile << recordingName << " & " << errorSum << " & " << flightErrorSum << " & " << reErrorSum << " & " << reFlightErrorSum << " \\\\ \\hline\n";
		cout << recordingName << endl;
		cout << "Abs. Mean Error: " << errorSum << endl;
		cout << "Abs. Mean Reprojection Error: " << reErrorSum << endl;
		cout << "Abs. Mean Error Flight: " << flightErrorSum << endl;
		cout << "Abs. Mean Reprojection Error Flight: " << reFlightErrorSum << endl;
		cout << endl;

		writeCSV(csvFile, e, 1.0 / recordingInfo.fps);
		
	}

	// Close Latex Table
	resultTableFile << 
		"\\end{tabular}\n" << 
		"\\end{center}\n";
	resultTableFile.close();

	// Close CSV files
	csvFile.close();
	csvFileFlight.close();
	
	cout << "Press q to quit." << endl;


	while (keyboard != 'q') {
		keyboard = cv::waitKey(10);
	}
}

void writeCSV(std::ofstream& myfile, EvaluationData ev, double timeBetweenFrames) {
	
	time_t maxOffset = timeBetweenFrames * 1000000000;
	for (Measurement<Ubitrack::Math::Vector3d> est : ev.trackingData_.measurements_) {
		writeCSVLine(myfile, ev, est.time(), timeBetweenFrames);
	}
}

void writeCSVLine(std::ofstream& myfile, EvaluationData ev, time_t timestamp, double timeBetweenFrames) {
	time_t maxOffset = timeBetweenFrames * 1000000000;
	Measurement<Ubitrack::Math::Vector3d> est;
	Measurement<double> absError, reError, distToCenterLeft, distToCenterRight;
	Measurement<Ubitrack::Math::Vector3d> error;
	if (ev.trackingData_.TryGetMeasurement(timestamp, maxOffset, est) &
		ev.absoluteErrors.TryGetMeasurement(timestamp, maxOffset, absError) && 
		ev.reprojectionErrors.TryGetMeasurement(timestamp, maxOffset, reError) && 
		ev.errors.TryGetMeasurement(timestamp, maxOffset, error))
		myfile << est.value() << ";" << abs(error.value()[0]) * 100 << ";" << abs(error.value()[1]) * 100 << ";" << abs(error.value()[2]) * 100  << ";" << absError.value() * 100 << ";" << reError.value() <<endl;
}
