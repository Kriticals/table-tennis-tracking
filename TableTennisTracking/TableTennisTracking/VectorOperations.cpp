#include "VectorOperations.h"

using namespace cv;

namespace TTT {


	Projection project(const std::vector<cv::Point>& contour, cv::Vec2f projectionAxis)
	{
		Projection p;
		p.min = projectionAxis.dot(cv::Vec2f(contour[0].x, contour[0].y));
		p.max = p.min;
		for (int z = 1; z < contour.size(); z++) {
			float val = projectionAxis.dot(cv::Vec2f(contour[z].x, contour[z].y));
			if (val < p.min)
				p.min = val;
			else if (val > p.max)
				p.max = val;
		}
		return p;
	}

	float projectionLength(const std::vector<cv::Point>& contour, cv::Vec2f projectionAxis)
	{
		return project(contour, projectionAxis).length();
	}

	float angleBetweenVectors(cv::Vec2f v1, cv::Vec2f v2)
	{
		float angle = 0.0f;
		float cosine = v1.dot(v2) / (vectorLength(v1) * vectorLength(v2));
		// cosine == 1 means angle is zero and acos(1) is undefined!
		if (cosine < 1)
			angle = acos(abs(cosine)) * 180.0 / M_PI;
		return angle;
	}

	cv::Vec2f normalVector(cv::Vec2f &v)
	{
		return cv::Vec2f(-v[1], v[0]);
	}

	bool vectorIntersection(cv::Vec2f v1, cv::Vec2f d1, cv::Vec2f v2, cv::Vec2f d2, cv::Vec2f &out)
	{
		// Perpendicular vector of v1
		Vec2f d1_normal = Vec2f(-d1[1], d1[0]);
		// Directional vector from v1 to v2
		Vec2f v1_v2 = v2 - v1;
		// Convert/project v2 coordinates to v1 coordinate system with d1 being the x-coordinate-axis
		float v2_x = d1.dot(v1_v2);
		float v2_y = d1_normal.dot(v1_v2);
		float d2_x = d1.dot(d2);
		float d2_y = d1_normal.dot(d2);
		// Directions are identical!
		if (d2_y == 0)
			return false;
		
		float a = v2_x - v2_y * d2_x / d2_y;

		out[0] = v1[0] + a * d1[0];
		out[1] = v1[1] + a * d1[1];

		return true;
	}

	inline float vectorLength(cv::Vec2f v)
	{
		return sqrt(v.dot(v));
	}

	inline float vectorLength(cv::Vec3f v)
	{
		return sqrt(v.dot(v));
	}

}

