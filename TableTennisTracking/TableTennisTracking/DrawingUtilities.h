#pragma once

#include "TrackedBall2D.h"

namespace TTT {

	enum TrackedBallDrawMode {
		CONTOUR,
		LINE,
		LINE_VERTICES
	};

	void drawTrackedBall(cv::UMat & inout, std::shared_ptr<TrackedBall2D> ball, TrackedBallDrawMode drawMode = LINE_VERTICES, cv::Scalar ballColor = cv::Scalar(0, 255, 0), cv::Scalar lineColor = cv::Scalar(200, 200, 200), cv::Scalar vertexColor = cv::Scalar(0, 0, 0));

	void drawSingleContour(cv::UMat &inout, const std::vector<cv::Point> &contour, cv::Scalar &lineColor);

	void drawRotatedRect(cv::UMat &inout, const cv::RotatedRect rRect, cv::Scalar color);

}
