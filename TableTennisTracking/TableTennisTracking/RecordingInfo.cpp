#include "RecordingInfo.h"

namespace TTT {


	RecordingInfo::RecordingInfo(std::string baseFolder, double exposure, double fps, time_t throwStartTime, time_t throwEndTime) :
		baseFolder(baseFolder),
		exposure(exposure),
		fps(fps),
		throwStartTime(throwStartTime),
		throwEndTime(throwEndTime)
	{
	}

	RecordingInfo::~RecordingInfo()
	{
	}


}
