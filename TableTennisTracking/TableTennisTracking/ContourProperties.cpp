#include "ContourProperties.h"

namespace TTT {

	using namespace std;
	using namespace cv;

	ContourProperties::ContourProperties(vector<cv::Point> contour, int frameId, time_t frameTime, OrientationApproximator orientationApproximator) :
		contour(contour),
		frameId(frameId),
		frameTime(frameTime),
		diameter(0.0f),
		radius(0.0f),
		length(0.0f),
		speedPixelsPerFrame(0.0f),
		area(0.0f),
		areaModelAprox(0.0f),
		assignedToBall(false)
	{
		RotatedRect minAreaBox = minAreaRect(contour);
		rotatedRect = minAreaBox;

		switch (orientationApproximator) {
		case PCA_:
		{
			//Construct a buffer used by the pca analysis
			int sz = static_cast<int>(contour.size());
			Mat data_pts = Mat(sz, 2, CV_64F);
			for (int j = 0; j < data_pts.rows; j++)
			{
				data_pts.at<double>(j, 0) = contour[j].x;
				data_pts.at<double>(j, 1) = contour[j].y;
			}
			//Perform PCA analysis
			PCA pca_analysis(data_pts, Mat(), PCA::DATA_AS_ROW);
			//Store the center of the object
			massCenter = Point2f(pca_analysis.mean.at<double>(0, 0),
				pca_analysis.mean.at<double>(0, 1));

			//Store the eigenvalues and eigenvectors
			vector<Point2f> eigen_vecs(2);
			vector<float> eigen_val(2);
			for (int i = 0; i < 2; i++)
			{
				eigen_vecs[i] = Point2f(pca_analysis.eigenvectors.at<double>(i, 0),
					pca_analysis.eigenvectors.at<double>(i, 1));
				eigen_val[i] = pca_analysis.eigenvalues.at<float>(i);
			}

			area = contourArea(contour);
			direction = Vec2f(eigen_vecs[0].x, eigen_vecs[0].y);
			diameter = projectionLength(contour, eigen_vecs[1]);
			length = projectionLength(contour, eigen_vecs[0]);
		}
		break;

		case MIN_AREA_RECT:
			Moments m = moments(contour);
			massCenter = Point2f(m.m10 / m.m00, m.m01 / m.m00);
			area = m.m00;

			Point2f points[4];
			minAreaBox.points(&points[0]);
			if (minAreaBox.size.width > minAreaBox.size.height) {
				diameter = minAreaBox.size.height;
				length = minAreaBox.size.width;
				direction = points[1] - points[2];
			}
			else {
				diameter = minAreaBox.size.width;
				length = minAreaBox.size.height;
				direction = points[1] - points[0];
			}

			break;
		}

		direction /= sqrt(direction.x * direction.x + direction.y * direction.y);
		radius = diameter / 2;
		speedPixelsPerFrame = (length - diameter);
		//speedPixelsPerSecond = speedPixelsPerFrame / diameter * Configuration::instance().realBallSize / Configuration::instance().exposure;
		// The expected area of a ball-streak is the bounding box minus the corners that are not filled by a circle/ball
		areaModelAprox = Configuration::instance().areaModelFactor * (diameter*length -
			(diameter * diameter - M_PI * radius * radius));
	}

	ContourProperties::~ContourProperties()
	{
	}

	void ContourProperties::print()
	{
		cout << "Mass Center: " << massCenter << endl;
		cout << "Radius: " << radius << endl;
		cout << "Length: " << length << endl;
		cout << "Speed (Frame): " << speedPixelsPerFrame << endl;
		cout << "Direction: " << direction << endl;
		cout << "Area: " << area << endl;
		cout << "Area Model Aprox: " << areaModelAprox << endl;
	}


}

